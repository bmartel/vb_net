﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEstimate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblCarPrice = New System.Windows.Forms.Label()
        Me.lblAccessories = New System.Windows.Forms.Label()
        Me.lblSubtotal = New System.Windows.Forms.Label()
        Me.lblSalesTax = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblTradeInAllowance = New System.Windows.Forms.Label()
        Me.lblAmountDue = New System.Windows.Forms.Label()
        Me.grpAccessories = New System.Windows.Forms.GroupBox()
        Me.chkComputerNavigation = New System.Windows.Forms.CheckBox()
        Me.chkLeatherInterior = New System.Windows.Forms.CheckBox()
        Me.chkStereoSystem = New System.Windows.Forms.CheckBox()
        Me.grpCarExteriorFinish = New System.Windows.Forms.GroupBox()
        Me.radCustom = New System.Windows.Forms.RadioButton()
        Me.radPearlized = New System.Windows.Forms.RadioButton()
        Me.radStandard = New System.Windows.Forms.RadioButton()
        Me.txtCustomer = New System.Windows.Forms.TextBox()
        Me.txtCarPrice = New System.Windows.Forms.TextBox()
        Me.txtTradeInAllowance = New System.Windows.Forms.TextBox()
        Me.mlblAccessories = New System.Windows.Forms.Label()
        Me.mlblSubtotal = New System.Windows.Forms.Label()
        Me.mlblSalesTax = New System.Windows.Forms.Label()
        Me.mlblTotal = New System.Windows.Forms.Label()
        Me.mlblAmountDue = New System.Windows.Forms.Label()
        Me.grpFinance = New System.Windows.Forms.GroupBox()
        Me.hsbarInterestRate = New System.Windows.Forms.HScrollBar()
        Me.hsbarNumYears = New System.Windows.Forms.HScrollBar()
        Me.lblMonthlyPaymentTxt = New System.Windows.Forms.Label()
        Me.lblInterestRateTxt = New System.Windows.Forms.Label()
        Me.lblNumYearsTxt = New System.Windows.Forms.Label()
        Me.lblMonthlyPayment = New System.Windows.Forms.Label()
        Me.lblInterestRate = New System.Windows.Forms.Label()
        Me.lblNumYears = New System.Windows.Forms.Label()
        Me.lblFormDiv = New System.Windows.Forms.Label()
        Me.ctxEstimate = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tspmCalculate = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmAcceptQuote = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmSummary = New System.Windows.Forms.ToolStripMenuItem()
        Me.grpAccessories.SuspendLayout()
        Me.grpCarExteriorFinish.SuspendLayout()
        Me.grpFinance.SuspendLayout()
        Me.ctxEstimate.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblCustomer
        '
        Me.lblCustomer.AutoSize = True
        Me.lblCustomer.Location = New System.Drawing.Point(32, 21)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(85, 13)
        Me.lblCustomer.TabIndex = 0
        Me.lblCustomer.Text = "C&ustomer Name:"
        '
        'lblCarPrice
        '
        Me.lblCarPrice.AutoSize = True
        Me.lblCarPrice.Location = New System.Drawing.Point(33, 58)
        Me.lblCarPrice.Name = "lblCarPrice"
        Me.lblCarPrice.Size = New System.Drawing.Size(84, 13)
        Me.lblCarPrice.TabIndex = 1
        Me.lblCarPrice.Text = "C&ar's Sale Price:"
        '
        'lblAccessories
        '
        Me.lblAccessories.AutoSize = True
        Me.lblAccessories.Location = New System.Drawing.Point(11, 94)
        Me.lblAccessories.Name = "lblAccessories"
        Me.lblAccessories.Size = New System.Drawing.Size(106, 13)
        Me.lblAccessories.TabIndex = 2
        Me.lblAccessories.Text = "Accessories && Finish:"
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(68, 126)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(49, 13)
        Me.lblSubtotal.TabIndex = 3
        Me.lblSubtotal.Text = "Subtotal:"
        '
        'lblSalesTax
        '
        Me.lblSalesTax.AutoSize = True
        Me.lblSalesTax.Location = New System.Drawing.Point(33, 161)
        Me.lblSalesTax.Name = "lblSalesTax"
        Me.lblSalesTax.Size = New System.Drawing.Size(86, 13)
        Me.lblSalesTax.TabIndex = 4
        Me.lblSalesTax.Text = "Sales Tax (12%):"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(83, 200)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 5
        Me.lblTotal.Text = "Total:"
        '
        'lblTradeInAllowance
        '
        Me.lblTradeInAllowance.AutoSize = True
        Me.lblTradeInAllowance.Location = New System.Drawing.Point(15, 236)
        Me.lblTradeInAllowance.Name = "lblTradeInAllowance"
        Me.lblTradeInAllowance.Size = New System.Drawing.Size(102, 13)
        Me.lblTradeInAllowance.TabIndex = 6
        Me.lblTradeInAllowance.Text = "T&rade-In Allowance:"
        '
        'lblAmountDue
        '
        Me.lblAmountDue.AutoSize = True
        Me.lblAmountDue.Location = New System.Drawing.Point(48, 272)
        Me.lblAmountDue.Name = "lblAmountDue"
        Me.lblAmountDue.Size = New System.Drawing.Size(69, 13)
        Me.lblAmountDue.TabIndex = 7
        Me.lblAmountDue.Text = "Amount Due:"
        '
        'grpAccessories
        '
        Me.grpAccessories.Controls.Add(Me.chkComputerNavigation)
        Me.grpAccessories.Controls.Add(Me.chkLeatherInterior)
        Me.grpAccessories.Controls.Add(Me.chkStereoSystem)
        Me.grpAccessories.Location = New System.Drawing.Point(302, 18)
        Me.grpAccessories.Name = "grpAccessories"
        Me.grpAccessories.Size = New System.Drawing.Size(200, 125)
        Me.grpAccessories.TabIndex = 8
        Me.grpAccessories.TabStop = False
        Me.grpAccessories.Text = "Accessories"
        '
        'chkComputerNavigation
        '
        Me.chkComputerNavigation.AutoSize = True
        Me.chkComputerNavigation.Location = New System.Drawing.Point(36, 91)
        Me.chkComputerNavigation.Name = "chkComputerNavigation"
        Me.chkComputerNavigation.Size = New System.Drawing.Size(125, 17)
        Me.chkComputerNavigation.TabIndex = 2
        Me.chkComputerNavigation.Text = "Computer &Navigation"
        Me.chkComputerNavigation.UseVisualStyleBackColor = True
        '
        'chkLeatherInterior
        '
        Me.chkLeatherInterior.AutoSize = True
        Me.chkLeatherInterior.Location = New System.Drawing.Point(36, 56)
        Me.chkLeatherInterior.Name = "chkLeatherInterior"
        Me.chkLeatherInterior.Size = New System.Drawing.Size(97, 17)
        Me.chkLeatherInterior.TabIndex = 1
        Me.chkLeatherInterior.Text = "Leather &Interior"
        Me.chkLeatherInterior.UseVisualStyleBackColor = True
        '
        'chkStereoSystem
        '
        Me.chkStereoSystem.AutoSize = True
        Me.chkStereoSystem.Location = New System.Drawing.Point(36, 21)
        Me.chkStereoSystem.Name = "chkStereoSystem"
        Me.chkStereoSystem.Size = New System.Drawing.Size(94, 17)
        Me.chkStereoSystem.TabIndex = 0
        Me.chkStereoSystem.Text = "&Stereo System"
        Me.chkStereoSystem.UseVisualStyleBackColor = True
        '
        'grpCarExteriorFinish
        '
        Me.grpCarExteriorFinish.Controls.Add(Me.radCustom)
        Me.grpCarExteriorFinish.Controls.Add(Me.radPearlized)
        Me.grpCarExteriorFinish.Controls.Add(Me.radStandard)
        Me.grpCarExteriorFinish.Location = New System.Drawing.Point(302, 173)
        Me.grpCarExteriorFinish.Name = "grpCarExteriorFinish"
        Me.grpCarExteriorFinish.Size = New System.Drawing.Size(200, 116)
        Me.grpCarExteriorFinish.TabIndex = 9
        Me.grpCarExteriorFinish.TabStop = False
        Me.grpCarExteriorFinish.Text = "Car Exterior Finish"
        '
        'radCustom
        '
        Me.radCustom.AutoSize = True
        Me.radCustom.Location = New System.Drawing.Point(36, 82)
        Me.radCustom.Name = "radCustom"
        Me.radCustom.Size = New System.Drawing.Size(123, 17)
        Me.radCustom.TabIndex = 2
        Me.radCustom.Text = "Customized &Detailing"
        Me.radCustom.UseVisualStyleBackColor = True
        '
        'radPearlized
        '
        Me.radPearlized.AutoSize = True
        Me.radPearlized.Location = New System.Drawing.Point(36, 51)
        Me.radPearlized.Name = "radPearlized"
        Me.radPearlized.Size = New System.Drawing.Size(68, 17)
        Me.radPearlized.TabIndex = 1
        Me.radPearlized.Text = "Pearli&zed"
        Me.radPearlized.UseVisualStyleBackColor = True
        '
        'radStandard
        '
        Me.radStandard.AutoSize = True
        Me.radStandard.Checked = True
        Me.radStandard.Location = New System.Drawing.Point(36, 20)
        Me.radStandard.Name = "radStandard"
        Me.radStandard.Size = New System.Drawing.Size(68, 17)
        Me.radStandard.TabIndex = 0
        Me.radStandard.TabStop = True
        Me.radStandard.Text = "S&tandard"
        Me.radStandard.UseVisualStyleBackColor = True
        '
        'txtCustomer
        '
        Me.txtCustomer.Location = New System.Drawing.Point(149, 18)
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.Size = New System.Drawing.Size(100, 20)
        Me.txtCustomer.TabIndex = 0
        '
        'txtCarPrice
        '
        Me.txtCarPrice.Location = New System.Drawing.Point(149, 55)
        Me.txtCarPrice.Name = "txtCarPrice"
        Me.txtCarPrice.Size = New System.Drawing.Size(100, 20)
        Me.txtCarPrice.TabIndex = 1
        Me.txtCarPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTradeInAllowance
        '
        Me.txtTradeInAllowance.Location = New System.Drawing.Point(149, 233)
        Me.txtTradeInAllowance.Name = "txtTradeInAllowance"
        Me.txtTradeInAllowance.Size = New System.Drawing.Size(100, 20)
        Me.txtTradeInAllowance.TabIndex = 6
        Me.txtTradeInAllowance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mlblAccessories
        '
        Me.mlblAccessories.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.mlblAccessories.Location = New System.Drawing.Point(149, 93)
        Me.mlblAccessories.MinimumSize = New System.Drawing.Size(90, 0)
        Me.mlblAccessories.Name = "mlblAccessories"
        Me.mlblAccessories.Size = New System.Drawing.Size(100, 20)
        Me.mlblAccessories.TabIndex = 13
        Me.mlblAccessories.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mlblSubtotal
        '
        Me.mlblSubtotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.mlblSubtotal.Location = New System.Drawing.Point(149, 125)
        Me.mlblSubtotal.MinimumSize = New System.Drawing.Size(90, 0)
        Me.mlblSubtotal.Name = "mlblSubtotal"
        Me.mlblSubtotal.Size = New System.Drawing.Size(100, 20)
        Me.mlblSubtotal.TabIndex = 14
        Me.mlblSubtotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mlblSalesTax
        '
        Me.mlblSalesTax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.mlblSalesTax.Location = New System.Drawing.Point(149, 160)
        Me.mlblSalesTax.MinimumSize = New System.Drawing.Size(90, 0)
        Me.mlblSalesTax.Name = "mlblSalesTax"
        Me.mlblSalesTax.Size = New System.Drawing.Size(100, 20)
        Me.mlblSalesTax.TabIndex = 15
        Me.mlblSalesTax.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mlblTotal
        '
        Me.mlblTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.mlblTotal.Location = New System.Drawing.Point(149, 199)
        Me.mlblTotal.Name = "mlblTotal"
        Me.mlblTotal.Size = New System.Drawing.Size(100, 20)
        Me.mlblTotal.TabIndex = 16
        Me.mlblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'mlblAmountDue
        '
        Me.mlblAmountDue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.mlblAmountDue.Location = New System.Drawing.Point(149, 271)
        Me.mlblAmountDue.Name = "mlblAmountDue"
        Me.mlblAmountDue.Size = New System.Drawing.Size(100, 20)
        Me.mlblAmountDue.TabIndex = 17
        Me.mlblAmountDue.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'grpFinance
        '
        Me.grpFinance.Controls.Add(Me.hsbarInterestRate)
        Me.grpFinance.Controls.Add(Me.hsbarNumYears)
        Me.grpFinance.Controls.Add(Me.lblMonthlyPaymentTxt)
        Me.grpFinance.Controls.Add(Me.lblInterestRateTxt)
        Me.grpFinance.Controls.Add(Me.lblNumYearsTxt)
        Me.grpFinance.Controls.Add(Me.lblMonthlyPayment)
        Me.grpFinance.Controls.Add(Me.lblInterestRate)
        Me.grpFinance.Controls.Add(Me.lblNumYears)
        Me.grpFinance.Location = New System.Drawing.Point(37, 336)
        Me.grpFinance.Name = "grpFinance"
        Me.grpFinance.Size = New System.Drawing.Size(451, 162)
        Me.grpFinance.TabIndex = 18
        Me.grpFinance.TabStop = False
        Me.grpFinance.Text = "Finance"
        '
        'hsbarInterestRate
        '
        Me.hsbarInterestRate.LargeChange = 25
        Me.hsbarInterestRate.Location = New System.Drawing.Point(179, 119)
        Me.hsbarInterestRate.Maximum = 2524
        Me.hsbarInterestRate.Name = "hsbarInterestRate"
        Me.hsbarInterestRate.Size = New System.Drawing.Size(80, 17)
        Me.hsbarInterestRate.TabIndex = 7
        '
        'hsbarNumYears
        '
        Me.hsbarNumYears.LargeChange = 3
        Me.hsbarNumYears.Location = New System.Drawing.Point(38, 119)
        Me.hsbarNumYears.Maximum = 12
        Me.hsbarNumYears.Minimum = 1
        Me.hsbarNumYears.Name = "hsbarNumYears"
        Me.hsbarNumYears.Size = New System.Drawing.Size(80, 17)
        Me.hsbarNumYears.TabIndex = 6
        Me.hsbarNumYears.Value = 1
        '
        'lblMonthlyPaymentTxt
        '
        Me.lblMonthlyPaymentTxt.AutoSize = True
        Me.lblMonthlyPaymentTxt.Location = New System.Drawing.Point(320, 92)
        Me.lblMonthlyPaymentTxt.Name = "lblMonthlyPaymentTxt"
        Me.lblMonthlyPaymentTxt.Size = New System.Drawing.Size(0, 13)
        Me.lblMonthlyPaymentTxt.TabIndex = 5
        '
        'lblInterestRateTxt
        '
        Me.lblInterestRateTxt.AutoSize = True
        Me.lblInterestRateTxt.Location = New System.Drawing.Point(179, 92)
        Me.lblInterestRateTxt.Name = "lblInterestRateTxt"
        Me.lblInterestRateTxt.Size = New System.Drawing.Size(0, 13)
        Me.lblInterestRateTxt.TabIndex = 4
        '
        'lblNumYearsTxt
        '
        Me.lblNumYearsTxt.AutoSize = True
        Me.lblNumYearsTxt.Location = New System.Drawing.Point(38, 92)
        Me.lblNumYearsTxt.Name = "lblNumYearsTxt"
        Me.lblNumYearsTxt.Size = New System.Drawing.Size(0, 13)
        Me.lblNumYearsTxt.TabIndex = 3
        '
        'lblMonthlyPayment
        '
        Me.lblMonthlyPayment.AutoSize = True
        Me.lblMonthlyPayment.Location = New System.Drawing.Point(317, 59)
        Me.lblMonthlyPayment.Name = "lblMonthlyPayment"
        Me.lblMonthlyPayment.Size = New System.Drawing.Size(88, 13)
        Me.lblMonthlyPayment.TabIndex = 2
        Me.lblMonthlyPayment.Text = "Monthly Payment"
        '
        'lblInterestRate
        '
        Me.lblInterestRate.AutoSize = True
        Me.lblInterestRate.Location = New System.Drawing.Point(176, 60)
        Me.lblInterestRate.Name = "lblInterestRate"
        Me.lblInterestRate.Size = New System.Drawing.Size(68, 13)
        Me.lblInterestRate.TabIndex = 1
        Me.lblInterestRate.Text = "Interest Rate"
        '
        'lblNumYears
        '
        Me.lblNumYears.AutoSize = True
        Me.lblNumYears.Location = New System.Drawing.Point(35, 61)
        Me.lblNumYears.Name = "lblNumYears"
        Me.lblNumYears.Size = New System.Drawing.Size(66, 13)
        Me.lblNumYears.TabIndex = 0
        Me.lblNumYears.Text = "No. of Years"
        '
        'lblFormDiv
        '
        Me.lblFormDiv.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblFormDiv.Location = New System.Drawing.Point(39, 315)
        Me.lblFormDiv.Name = "lblFormDiv"
        Me.lblFormDiv.Size = New System.Drawing.Size(449, 3)
        Me.lblFormDiv.TabIndex = 19
        '
        'ctxEstimate
        '
        Me.ctxEstimate.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tspmCalculate, Me.tspmAcceptQuote, Me.tspmClear, Me.tspmSummary})
        Me.ctxEstimate.Name = "ctxEstimate"
        Me.ctxEstimate.Size = New System.Drawing.Size(148, 92)
        '
        'tspmCalculate
        '
        Me.tspmCalculate.Name = "tspmCalculate"
        Me.tspmCalculate.Size = New System.Drawing.Size(147, 22)
        Me.tspmCalculate.Text = "Calculate"
        '
        'tspmAcceptQuote
        '
        Me.tspmAcceptQuote.Name = "tspmAcceptQuote"
        Me.tspmAcceptQuote.Size = New System.Drawing.Size(147, 22)
        Me.tspmAcceptQuote.Text = "Accept Quote"
        '
        'tspmClear
        '
        Me.tspmClear.Name = "tspmClear"
        Me.tspmClear.Size = New System.Drawing.Size(147, 22)
        Me.tspmClear.Text = "Clear"
        '
        'tspmSummary
        '
        Me.tspmSummary.Name = "tspmSummary"
        Me.tspmSummary.Size = New System.Drawing.Size(147, 22)
        Me.tspmSummary.Text = "Summary"
        '
        'frmEstimate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 519)
        Me.ContextMenuStrip = Me.ctxEstimate
        Me.Controls.Add(Me.lblFormDiv)
        Me.Controls.Add(Me.grpFinance)
        Me.Controls.Add(Me.mlblAmountDue)
        Me.Controls.Add(Me.mlblTotal)
        Me.Controls.Add(Me.mlblSalesTax)
        Me.Controls.Add(Me.mlblSubtotal)
        Me.Controls.Add(Me.mlblAccessories)
        Me.Controls.Add(Me.txtTradeInAllowance)
        Me.Controls.Add(Me.txtCarPrice)
        Me.Controls.Add(Me.txtCustomer)
        Me.Controls.Add(Me.grpCarExteriorFinish)
        Me.Controls.Add(Me.grpAccessories)
        Me.Controls.Add(Me.lblAmountDue)
        Me.Controls.Add(Me.lblTradeInAllowance)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblSalesTax)
        Me.Controls.Add(Me.lblSubtotal)
        Me.Controls.Add(Me.lblAccessories)
        Me.Controls.Add(Me.lblCarPrice)
        Me.Controls.Add(Me.lblCustomer)
        Me.Name = "frmEstimate"
        Me.Text = "VB Auto Centre – Estimate"
        Me.grpAccessories.ResumeLayout(False)
        Me.grpAccessories.PerformLayout()
        Me.grpCarExteriorFinish.ResumeLayout(False)
        Me.grpCarExteriorFinish.PerformLayout()
        Me.grpFinance.ResumeLayout(False)
        Me.grpFinance.PerformLayout()
        Me.ctxEstimate.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblCarPrice As System.Windows.Forms.Label
    Friend WithEvents lblAccessories As System.Windows.Forms.Label
    Friend WithEvents lblSubtotal As System.Windows.Forms.Label
    Friend WithEvents lblSalesTax As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblTradeInAllowance As System.Windows.Forms.Label
    Friend WithEvents lblAmountDue As System.Windows.Forms.Label
    Friend WithEvents grpAccessories As System.Windows.Forms.GroupBox
    Friend WithEvents grpCarExteriorFinish As System.Windows.Forms.GroupBox
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents txtCarPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtTradeInAllowance As System.Windows.Forms.TextBox
    Friend WithEvents chkComputerNavigation As System.Windows.Forms.CheckBox
    Friend WithEvents chkLeatherInterior As System.Windows.Forms.CheckBox
    Friend WithEvents chkStereoSystem As System.Windows.Forms.CheckBox
    Friend WithEvents radCustom As System.Windows.Forms.RadioButton
    Friend WithEvents radPearlized As System.Windows.Forms.RadioButton
    Friend WithEvents radStandard As System.Windows.Forms.RadioButton
    Friend WithEvents mlblAccessories As System.Windows.Forms.Label
    Friend WithEvents mlblSubtotal As System.Windows.Forms.Label
    Friend WithEvents mlblSalesTax As System.Windows.Forms.Label
    Friend WithEvents mlblTotal As System.Windows.Forms.Label
    Friend WithEvents mlblAmountDue As System.Windows.Forms.Label
    Friend WithEvents grpFinance As System.Windows.Forms.GroupBox
    Friend WithEvents lblMonthlyPayment As System.Windows.Forms.Label
    Friend WithEvents lblInterestRate As System.Windows.Forms.Label
    Friend WithEvents lblNumYears As System.Windows.Forms.Label
    Friend WithEvents lblFormDiv As System.Windows.Forms.Label
    Friend WithEvents lblMonthlyPaymentTxt As System.Windows.Forms.Label
    Friend WithEvents lblInterestRateTxt As System.Windows.Forms.Label
    Friend WithEvents lblNumYearsTxt As System.Windows.Forms.Label
    Friend WithEvents hsbarInterestRate As System.Windows.Forms.HScrollBar
    Friend WithEvents hsbarNumYears As System.Windows.Forms.HScrollBar
    Friend WithEvents ctxEstimate As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tspmCalculate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmAcceptQuote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmClear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmSummary As System.Windows.Forms.ToolStripMenuItem
End Class
