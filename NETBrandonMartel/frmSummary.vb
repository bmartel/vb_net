﻿Public Class frmSummary

    Private Sub frmSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblTotalServiceContractsTxt.Text = FormatCurrency(BusinessTier.clsService.AccumulatedTotals, 2)
        lblNumContractsTxt.Text = BusinessTier.clsService.NumberOfContracts
        lblTotalPstTxt.Text = FormatCurrency(BusinessTier.clsService.AccumulatedPst, 2)
        lblTotalGstTxt.Text = FormatCurrency(BusinessTier.clsService.AccumulatedGst, 2)
        lblAvgServiceContractsTxt.Text = FormatCurrency(BusinessTier.clsService.averageContract, 2)
    End Sub
End Class
