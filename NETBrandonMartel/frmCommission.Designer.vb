﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCommission
    Inherits NETBrandonMartel.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgrCommissions = New System.Windows.Forms.DataGridView()
        CType(Me.dgrCommissions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Location = New System.Drawing.Point(255, 33)
        '
        'lblLine1
        '
        Me.lblLine1.Location = New System.Drawing.Point(41, 351)
        Me.lblLine1.Size = New System.Drawing.Size(545, 3)
        '
        'lblLine3
        '
        Me.lblLine3.Location = New System.Drawing.Point(41, 391)
        Me.lblLine3.Size = New System.Drawing.Size(465, 3)
        '
        'lblLine2
        '
        Me.lblLine2.Location = New System.Drawing.Point(41, 371)
        Me.lblLine2.Size = New System.Drawing.Size(465, 3)
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(516, 371)
        Me.btnExit.Size = New System.Drawing.Size(71, 23)
        '
        'dgrCommissions
        '
        Me.dgrCommissions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrCommissions.Location = New System.Drawing.Point(44, 81)
        Me.dgrCommissions.Name = "dgrCommissions"
        Me.dgrCommissions.ReadOnly = True
        Me.dgrCommissions.Size = New System.Drawing.Size(542, 238)
        Me.dgrCommissions.TabIndex = 5
        '
        'frmCommission
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(648, 432)
        Me.Controls.Add(Me.dgrCommissions)
        Me.Name = "frmCommission"
        Me.Text = "Commission Listing"
        Me.Controls.SetChildIndex(Me.lblTitle, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.lblLine1, 0)
        Me.Controls.SetChildIndex(Me.lblLine2, 0)
        Me.Controls.SetChildIndex(Me.lblLine3, 0)
        Me.Controls.SetChildIndex(Me.dgrCommissions, 0)
        CType(Me.dgrCommissions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgrCommissions As System.Windows.Forms.DataGridView

End Class
