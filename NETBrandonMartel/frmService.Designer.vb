﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmService
    Inherits NETBrandonMartel.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblServiceDesc = New System.Windows.Forms.Label()
        Me.lblPrice = New System.Windows.Forms.Label()
        Me.lblParts = New System.Windows.Forms.Label()
        Me.lblMaterials = New System.Windows.Forms.Label()
        Me.lblPst = New System.Windows.Forms.Label()
        Me.lblGst = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblPstTxt = New System.Windows.Forms.Label()
        Me.lblGstTxt = New System.Windows.Forms.Label()
        Me.lblTotalTxt = New System.Windows.Forms.Label()
        Me.txtServiceDesc = New System.Windows.Forms.TextBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.txtParts = New System.Windows.Forms.TextBox()
        Me.txtMaterials = New System.Windows.Forms.TextBox()
        Me.ctxService = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ctsmCalc = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctsmClear = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ctsmSummary = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctxService.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblLine1
        '
        Me.lblLine1.Location = New System.Drawing.Point(45, 353)
        '
        'lblLine3
        '
        Me.lblLine3.Location = New System.Drawing.Point(45, 393)
        '
        'lblLine2
        '
        Me.lblLine2.Location = New System.Drawing.Point(45, 373)
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(301, 373)
        Me.btnExit.TabIndex = 4
        '
        'lblServiceDesc
        '
        Me.lblServiceDesc.AutoSize = True
        Me.lblServiceDesc.Location = New System.Drawing.Point(45, 89)
        Me.lblServiceDesc.Name = "lblServiceDesc"
        Me.lblServiceDesc.Size = New System.Drawing.Size(102, 13)
        Me.lblServiceDesc.TabIndex = 5
        Me.lblServiceDesc.Text = "Service Description:"
        '
        'lblPrice
        '
        Me.lblPrice.AutoSize = True
        Me.lblPrice.Location = New System.Drawing.Point(113, 124)
        Me.lblPrice.Name = "lblPrice"
        Me.lblPrice.Size = New System.Drawing.Size(34, 13)
        Me.lblPrice.TabIndex = 6
        Me.lblPrice.Text = "Price:"
        '
        'lblParts
        '
        Me.lblParts.AutoSize = True
        Me.lblParts.Location = New System.Drawing.Point(113, 158)
        Me.lblParts.Name = "lblParts"
        Me.lblParts.Size = New System.Drawing.Size(34, 13)
        Me.lblParts.TabIndex = 7
        Me.lblParts.Text = "Parts:"
        '
        'lblMaterials
        '
        Me.lblMaterials.AutoSize = True
        Me.lblMaterials.Location = New System.Drawing.Point(72, 192)
        Me.lblMaterials.Name = "lblMaterials"
        Me.lblMaterials.Size = New System.Drawing.Size(75, 13)
        Me.lblMaterials.TabIndex = 8
        Me.lblMaterials.Text = "Shop Material:"
        '
        'lblPst
        '
        Me.lblPst.AutoSize = True
        Me.lblPst.Location = New System.Drawing.Point(116, 224)
        Me.lblPst.Name = "lblPst"
        Me.lblPst.Size = New System.Drawing.Size(31, 13)
        Me.lblPst.TabIndex = 9
        Me.lblPst.Text = "PST:"
        '
        'lblGst
        '
        Me.lblGst.AutoSize = True
        Me.lblGst.Location = New System.Drawing.Point(115, 257)
        Me.lblGst.Name = "lblGst"
        Me.lblGst.Size = New System.Drawing.Size(32, 13)
        Me.lblGst.TabIndex = 10
        Me.lblGst.Text = "GST:"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(113, 291)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(34, 13)
        Me.lblTotal.TabIndex = 11
        Me.lblTotal.Text = "Total:"
        '
        'lblPstTxt
        '
        Me.lblPstTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPstTxt.Location = New System.Drawing.Point(199, 223)
        Me.lblPstTxt.Name = "lblPstTxt"
        Me.lblPstTxt.Size = New System.Drawing.Size(100, 21)
        Me.lblPstTxt.TabIndex = 4
        Me.lblPstTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblGstTxt
        '
        Me.lblGstTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblGstTxt.Location = New System.Drawing.Point(199, 256)
        Me.lblGstTxt.Name = "lblGstTxt"
        Me.lblGstTxt.Size = New System.Drawing.Size(100, 21)
        Me.lblGstTxt.TabIndex = 13
        Me.lblGstTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotalTxt
        '
        Me.lblTotalTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalTxt.Location = New System.Drawing.Point(199, 290)
        Me.lblTotalTxt.Name = "lblTotalTxt"
        Me.lblTotalTxt.Size = New System.Drawing.Size(100, 21)
        Me.lblTotalTxt.TabIndex = 14
        Me.lblTotalTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtServiceDesc
        '
        Me.txtServiceDesc.Location = New System.Drawing.Point(199, 86)
        Me.txtServiceDesc.Name = "txtServiceDesc"
        Me.txtServiceDesc.Size = New System.Drawing.Size(176, 20)
        Me.txtServiceDesc.TabIndex = 0
        '
        'txtPrice
        '
        Me.txtPrice.Location = New System.Drawing.Point(199, 121)
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(100, 20)
        Me.txtPrice.TabIndex = 1
        Me.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtParts
        '
        Me.txtParts.Location = New System.Drawing.Point(199, 155)
        Me.txtParts.Name = "txtParts"
        Me.txtParts.Size = New System.Drawing.Size(100, 20)
        Me.txtParts.TabIndex = 2
        Me.txtParts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMaterials
        '
        Me.txtMaterials.Location = New System.Drawing.Point(199, 189)
        Me.txtMaterials.Name = "txtMaterials"
        Me.txtMaterials.Size = New System.Drawing.Size(100, 20)
        Me.txtMaterials.TabIndex = 3
        Me.txtMaterials.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'ctxService
        '
        Me.ctxService.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ctsmCalc, Me.ctsmClear, Me.ToolStripMenuItem1, Me.ctsmSummary})
        Me.ctxService.Name = "ctxService"
        Me.ctxService.Size = New System.Drawing.Size(126, 76)
        '
        'ctsmCalc
        '
        Me.ctsmCalc.Enabled = False
        Me.ctsmCalc.Name = "ctsmCalc"
        Me.ctsmCalc.Size = New System.Drawing.Size(125, 22)
        Me.ctsmCalc.Text = "Calculate"
        '
        'ctsmClear
        '
        Me.ctsmClear.Name = "ctsmClear"
        Me.ctsmClear.Size = New System.Drawing.Size(125, 22)
        Me.ctsmClear.Text = "Clear"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(122, 6)
        '
        'ctsmSummary
        '
        Me.ctsmSummary.Enabled = False
        Me.ctsmSummary.Name = "ctsmSummary"
        Me.ctsmSummary.Size = New System.Drawing.Size(125, 22)
        Me.ctsmSummary.Text = "Summary"
        '
        'frmService
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(406, 417)
        Me.ContextMenuStrip = Me.ctxService
        Me.Controls.Add(Me.txtMaterials)
        Me.Controls.Add(Me.txtParts)
        Me.Controls.Add(Me.txtPrice)
        Me.Controls.Add(Me.txtServiceDesc)
        Me.Controls.Add(Me.lblTotalTxt)
        Me.Controls.Add(Me.lblGstTxt)
        Me.Controls.Add(Me.lblPstTxt)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblGst)
        Me.Controls.Add(Me.lblPst)
        Me.Controls.Add(Me.lblMaterials)
        Me.Controls.Add(Me.lblParts)
        Me.Controls.Add(Me.lblPrice)
        Me.Controls.Add(Me.lblServiceDesc)
        Me.Name = "frmService"
        Me.Text = "VbAuto Service"
        Me.Controls.SetChildIndex(Me.lblTitle, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.lblLine1, 0)
        Me.Controls.SetChildIndex(Me.lblLine2, 0)
        Me.Controls.SetChildIndex(Me.lblLine3, 0)
        Me.Controls.SetChildIndex(Me.lblServiceDesc, 0)
        Me.Controls.SetChildIndex(Me.lblPrice, 0)
        Me.Controls.SetChildIndex(Me.lblParts, 0)
        Me.Controls.SetChildIndex(Me.lblMaterials, 0)
        Me.Controls.SetChildIndex(Me.lblPst, 0)
        Me.Controls.SetChildIndex(Me.lblGst, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.lblPstTxt, 0)
        Me.Controls.SetChildIndex(Me.lblGstTxt, 0)
        Me.Controls.SetChildIndex(Me.lblTotalTxt, 0)
        Me.Controls.SetChildIndex(Me.txtServiceDesc, 0)
        Me.Controls.SetChildIndex(Me.txtPrice, 0)
        Me.Controls.SetChildIndex(Me.txtParts, 0)
        Me.Controls.SetChildIndex(Me.txtMaterials, 0)
        Me.ctxService.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblServiceDesc As System.Windows.Forms.Label
    Friend WithEvents lblPrice As System.Windows.Forms.Label
    Friend WithEvents lblParts As System.Windows.Forms.Label
    Friend WithEvents lblMaterials As System.Windows.Forms.Label
    Friend WithEvents lblPst As System.Windows.Forms.Label
    Friend WithEvents lblGst As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblPstTxt As System.Windows.Forms.Label
    Friend WithEvents lblGstTxt As System.Windows.Forms.Label
    Friend WithEvents lblTotalTxt As System.Windows.Forms.Label
    Friend WithEvents txtServiceDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtParts As System.Windows.Forms.TextBox
    Friend WithEvents txtMaterials As System.Windows.Forms.TextBox
    Friend WithEvents ctxService As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ctsmCalc As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ctsmClear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ctsmSummary As System.Windows.Forms.ToolStripMenuItem

End Class
