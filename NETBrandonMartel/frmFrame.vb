﻿Public Class frmFrame
    ''' <summary>
    ''' Opens a new estimate window when menu item is clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Private Sub mnuEstimate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEstimate.Click, tspmEstimate.Click
        'creates a new instance of the estimate window object
        Dim frmInstance As New frmEstimate

        'sets instance to be a child of the MDI frmFrame, and displays it
        frmInstance.MdiParent = Me
        frmInstance.Show()
    End Sub
    ''' <summary>
    ''' Arranges open forms in MDI window by stacking them horizontal
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspbTile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspbTile.Click, mnuTile.Click
        Me.LayoutMdi(System.Windows.Forms.MdiLayout.TileHorizontal)
    End Sub
    ''' <summary>
    ''' Arranges open forms in MDI window by stacking them vertical
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspbLayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspbLayer.Click, mnuLayer.Click
        Me.LayoutMdi(System.Windows.Forms.MdiLayout.TileVertical)
    End Sub
    ''' <summary>
    ''' Arranges open forms in MDI window by layering them in a cascade
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspbCascade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspbCascade.Click, mnuCascade.Click
        Me.LayoutMdi(System.Windows.Forms.MdiLayout.Cascade)
    End Sub
    ''' <summary>
    ''' Exits current program
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspbExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspbExit.Click, mnuExit.Click
        Me.Close()
    End Sub
    ''' <summary>
    ''' To be coded at a later time
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspbHelp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspbHelp.Click
        'TODO code
    End Sub

    Private Sub frmFrame_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub mnuService_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuService.Click, tspmService.Click
        Dim frmServiceInstance As frmService = New frmService

        frmServiceInstance.MdiParent = Me
        frmServiceInstance.Show()
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click, tspbHelp.Click
        Dim frmAboutInstance As frmAbout = New frmAbout
        frmAboutInstance.ShowDialog()
    End Sub

    Private Sub mnuCarWash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCarWash.Click, tspmCarWash.Click
        Dim frmCarWashInstance As frmCarWash = New frmCarWash

        frmCarWashInstance.MdiParent = Me
        frmCarWashInstance.Show()
    End Sub

    Private Sub mnuCommission_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCommission.Click, tspmCommission.Click
        Dim frmVBAutoInstance As frmVBAuto = New frmVBAuto

        frmVBAutoInstance.MdiParent = Me
        frmVBAutoInstance.Show()
    End Sub

    Private Sub mnuReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReports.Click, tspmReports.Click
        Dim frmRptInstance As frmReport = New frmReport

        frmRptInstance.MdiParent = Me
        frmRptInstance.Show()
    End Sub
End Class
