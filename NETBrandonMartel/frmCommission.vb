﻿Public Class frmCommission

    Dim mobjData As New DataTier.clsData
    Dim mdsCommission As New DataSet

    ''' <summary>
    ''' instantiate all necessary datasets, adapters and connections to populate the datagrid on load of form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmCommission_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'connect to the auto sales db
            mobjData.Connect()

            'capture the commissions data set
            mdsCommission = mobjData.getCommission()

            'bind the data set data to the datagrid view
            dgrCommissions.DataSource = mdsCommission.Tables("Commissions")
        Catch ex As Exception
            sendMessage("There was an error " & ControlChars.NewLine & ex.Message, "Data Handling Exception")
        End Try

    End Sub


End Class
