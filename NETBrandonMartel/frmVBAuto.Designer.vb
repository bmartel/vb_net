﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVBAuto
    Inherits NETBrandonMartel.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grpSalesPersonInfo = New System.Windows.Forms.GroupBox()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.lblDealerCodeTxt = New System.Windows.Forms.Label()
        Me.lblSalesPersonNameTxt = New System.Windows.Forms.Label()
        Me.lblSalesPersonTxt = New System.Windows.Forms.Label()
        Me.lblDealerCode = New System.Windows.Forms.Label()
        Me.lblSalesPersonName = New System.Windows.Forms.Label()
        Me.lblSalesPerson = New System.Windows.Forms.Label()
        Me.grpStockInfo = New System.Windows.Forms.GroupBox()
        Me.btnCommission = New System.Windows.Forms.Button()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.cboStockNumber = New System.Windows.Forms.ComboBox()
        Me.txtRetailPrice = New System.Windows.Forms.TextBox()
        Me.txtCostPrice = New System.Windows.Forms.TextBox()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.lblCommissionTxt = New System.Windows.Forms.Label()
        Me.lblCommission = New System.Windows.Forms.Label()
        Me.lblRetailPrice = New System.Windows.Forms.Label()
        Me.lblCostPrice = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.lblStockNo = New System.Windows.Forms.Label()
        Me.grpSalesPersonInfo.SuspendLayout()
        Me.grpStockInfo.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Location = New System.Drawing.Point(159, 33)
        '
        'lblLine1
        '
        Me.lblLine1.Location = New System.Drawing.Point(60, 629)
        Me.lblLine1.Size = New System.Drawing.Size(368, 3)
        '
        'lblLine3
        '
        Me.lblLine3.Location = New System.Drawing.Point(60, 669)
        Me.lblLine3.Size = New System.Drawing.Size(288, 3)
        '
        'lblLine2
        '
        Me.lblLine2.Location = New System.Drawing.Point(60, 649)
        Me.lblLine2.Size = New System.Drawing.Size(288, 3)
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(354, 649)
        '
        'grpSalesPersonInfo
        '
        Me.grpSalesPersonInfo.Controls.Add(Me.btnNext)
        Me.grpSalesPersonInfo.Controls.Add(Me.btnPrevious)
        Me.grpSalesPersonInfo.Controls.Add(Me.lblDealerCodeTxt)
        Me.grpSalesPersonInfo.Controls.Add(Me.lblSalesPersonNameTxt)
        Me.grpSalesPersonInfo.Controls.Add(Me.lblSalesPersonTxt)
        Me.grpSalesPersonInfo.Controls.Add(Me.lblDealerCode)
        Me.grpSalesPersonInfo.Controls.Add(Me.lblSalesPersonName)
        Me.grpSalesPersonInfo.Controls.Add(Me.lblSalesPerson)
        Me.grpSalesPersonInfo.Location = New System.Drawing.Point(39, 81)
        Me.grpSalesPersonInfo.Name = "grpSalesPersonInfo"
        Me.grpSalesPersonInfo.Size = New System.Drawing.Size(390, 189)
        Me.grpSalesPersonInfo.TabIndex = 5
        Me.grpSalesPersonInfo.TabStop = False
        Me.grpSalesPersonInfo.Text = "Sales Person Info"
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(274, 154)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.TabIndex = 15
        Me.btnNext.Text = "&Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Location = New System.Drawing.Point(179, 154)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(75, 23)
        Me.btnPrevious.TabIndex = 14
        Me.btnPrevious.Text = "&Previous"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'lblDealerCodeTxt
        '
        Me.lblDealerCodeTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDealerCodeTxt.Location = New System.Drawing.Point(179, 108)
        Me.lblDealerCodeTxt.Name = "lblDealerCodeTxt"
        Me.lblDealerCodeTxt.Size = New System.Drawing.Size(173, 23)
        Me.lblDealerCodeTxt.TabIndex = 5
        '
        'lblSalesPersonNameTxt
        '
        Me.lblSalesPersonNameTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSalesPersonNameTxt.Location = New System.Drawing.Point(179, 71)
        Me.lblSalesPersonNameTxt.Name = "lblSalesPersonNameTxt"
        Me.lblSalesPersonNameTxt.Size = New System.Drawing.Size(173, 23)
        Me.lblSalesPersonNameTxt.TabIndex = 4
        '
        'lblSalesPersonTxt
        '
        Me.lblSalesPersonTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSalesPersonTxt.Location = New System.Drawing.Point(179, 33)
        Me.lblSalesPersonTxt.Name = "lblSalesPersonTxt"
        Me.lblSalesPersonTxt.Size = New System.Drawing.Size(173, 23)
        Me.lblSalesPersonTxt.TabIndex = 3
        '
        'lblDealerCode
        '
        Me.lblDealerCode.AutoSize = True
        Me.lblDealerCode.Location = New System.Drawing.Point(42, 108)
        Me.lblDealerCode.Name = "lblDealerCode"
        Me.lblDealerCode.Size = New System.Drawing.Size(69, 13)
        Me.lblDealerCode.TabIndex = 2
        Me.lblDealerCode.Text = "Dealer Code:"
        '
        'lblSalesPersonName
        '
        Me.lblSalesPersonName.AutoSize = True
        Me.lblSalesPersonName.Location = New System.Drawing.Point(42, 71)
        Me.lblSalesPersonName.Name = "lblSalesPersonName"
        Me.lblSalesPersonName.Size = New System.Drawing.Size(103, 13)
        Me.lblSalesPersonName.TabIndex = 1
        Me.lblSalesPersonName.Text = "Sales Person Name:"
        '
        'lblSalesPerson
        '
        Me.lblSalesPerson.AutoSize = True
        Me.lblSalesPerson.Location = New System.Drawing.Point(42, 33)
        Me.lblSalesPerson.Name = "lblSalesPerson"
        Me.lblSalesPerson.Size = New System.Drawing.Size(86, 13)
        Me.lblSalesPerson.TabIndex = 0
        Me.lblSalesPerson.Text = "Sales Person ID:"
        '
        'grpStockInfo
        '
        Me.grpStockInfo.Controls.Add(Me.btnCommission)
        Me.grpStockInfo.Controls.Add(Me.btnCalculate)
        Me.grpStockInfo.Controls.Add(Me.cboStockNumber)
        Me.grpStockInfo.Controls.Add(Me.txtRetailPrice)
        Me.grpStockInfo.Controls.Add(Me.txtCostPrice)
        Me.grpStockInfo.Controls.Add(Me.txtDescription)
        Me.grpStockInfo.Controls.Add(Me.lblCommissionTxt)
        Me.grpStockInfo.Controls.Add(Me.lblCommission)
        Me.grpStockInfo.Controls.Add(Me.lblRetailPrice)
        Me.grpStockInfo.Controls.Add(Me.lblCostPrice)
        Me.grpStockInfo.Controls.Add(Me.lblDescription)
        Me.grpStockInfo.Controls.Add(Me.lblStockNo)
        Me.grpStockInfo.Location = New System.Drawing.Point(39, 276)
        Me.grpStockInfo.Name = "grpStockInfo"
        Me.grpStockInfo.Size = New System.Drawing.Size(390, 338)
        Me.grpStockInfo.TabIndex = 6
        Me.grpStockInfo.TabStop = False
        Me.grpStockInfo.Text = "Stock Information"
        '
        'btnCommission
        '
        Me.btnCommission.Location = New System.Drawing.Point(277, 306)
        Me.btnCommission.Name = "btnCommission"
        Me.btnCommission.Size = New System.Drawing.Size(75, 23)
        Me.btnCommission.TabIndex = 13
        Me.btnCommission.Text = "View Comm"
        Me.btnCommission.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(182, 306)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(75, 23)
        Me.btnCalculate.TabIndex = 12
        Me.btnCalculate.Text = "Ca&lculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'cboStockNumber
        '
        Me.cboStockNumber.FormattingEnabled = True
        Me.cboStockNumber.Location = New System.Drawing.Point(182, 50)
        Me.cboStockNumber.Name = "cboStockNumber"
        Me.cboStockNumber.Size = New System.Drawing.Size(170, 21)
        Me.cboStockNumber.TabIndex = 11
        '
        'txtRetailPrice
        '
        Me.txtRetailPrice.Location = New System.Drawing.Point(182, 202)
        Me.txtRetailPrice.Name = "txtRetailPrice"
        Me.txtRetailPrice.Size = New System.Drawing.Size(170, 20)
        Me.txtRetailPrice.TabIndex = 10
        Me.txtRetailPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCostPrice
        '
        Me.txtCostPrice.Location = New System.Drawing.Point(182, 153)
        Me.txtCostPrice.Name = "txtCostPrice"
        Me.txtCostPrice.Size = New System.Drawing.Size(170, 20)
        Me.txtCostPrice.TabIndex = 9
        Me.txtCostPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtDescription
        '
        Me.txtDescription.Location = New System.Drawing.Point(182, 106)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(170, 20)
        Me.txtDescription.TabIndex = 8
        '
        'lblCommissionTxt
        '
        Me.lblCommissionTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCommissionTxt.Location = New System.Drawing.Point(182, 253)
        Me.lblCommissionTxt.Name = "lblCommissionTxt"
        Me.lblCommissionTxt.Size = New System.Drawing.Size(170, 23)
        Me.lblCommissionTxt.TabIndex = 6
        Me.lblCommissionTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblCommission
        '
        Me.lblCommission.AutoSize = True
        Me.lblCommission.Location = New System.Drawing.Point(42, 253)
        Me.lblCommission.Name = "lblCommission"
        Me.lblCommission.Size = New System.Drawing.Size(65, 13)
        Me.lblCommission.TabIndex = 5
        Me.lblCommission.Text = "Commission:"
        '
        'lblRetailPrice
        '
        Me.lblRetailPrice.AutoSize = True
        Me.lblRetailPrice.Location = New System.Drawing.Point(42, 205)
        Me.lblRetailPrice.Name = "lblRetailPrice"
        Me.lblRetailPrice.Size = New System.Drawing.Size(64, 13)
        Me.lblRetailPrice.TabIndex = 4
        Me.lblRetailPrice.Text = "&Retail Price:"
        '
        'lblCostPrice
        '
        Me.lblCostPrice.AutoSize = True
        Me.lblCostPrice.Location = New System.Drawing.Point(42, 156)
        Me.lblCostPrice.Name = "lblCostPrice"
        Me.lblCostPrice.Size = New System.Drawing.Size(58, 13)
        Me.lblCostPrice.TabIndex = 3
        Me.lblCostPrice.Text = "&Cost Price:"
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(42, 109)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(63, 13)
        Me.lblDescription.TabIndex = 2
        Me.lblDescription.Text = "&Description:"
        '
        'lblStockNo
        '
        Me.lblStockNo.AutoSize = True
        Me.lblStockNo.Location = New System.Drawing.Point(42, 59)
        Me.lblStockNo.Name = "lblStockNo"
        Me.lblStockNo.Size = New System.Drawing.Size(48, 13)
        Me.lblStockNo.TabIndex = 1
        Me.lblStockNo.Text = "Stock #:"
        '
        'frmVBAuto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(468, 688)
        Me.Controls.Add(Me.grpStockInfo)
        Me.Controls.Add(Me.grpSalesPersonInfo)
        Me.Name = "frmVBAuto"
        Me.Text = "VB Auto Commission"
        Me.Controls.SetChildIndex(Me.lblTitle, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.lblLine1, 0)
        Me.Controls.SetChildIndex(Me.lblLine2, 0)
        Me.Controls.SetChildIndex(Me.lblLine3, 0)
        Me.Controls.SetChildIndex(Me.grpSalesPersonInfo, 0)
        Me.Controls.SetChildIndex(Me.grpStockInfo, 0)
        Me.grpSalesPersonInfo.ResumeLayout(False)
        Me.grpSalesPersonInfo.PerformLayout()
        Me.grpStockInfo.ResumeLayout(False)
        Me.grpStockInfo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grpSalesPersonInfo As System.Windows.Forms.GroupBox
    Friend WithEvents grpStockInfo As System.Windows.Forms.GroupBox
    Friend WithEvents lblDealerCodeTxt As System.Windows.Forms.Label
    Friend WithEvents lblSalesPersonNameTxt As System.Windows.Forms.Label
    Friend WithEvents lblSalesPersonTxt As System.Windows.Forms.Label
    Friend WithEvents lblDealerCode As System.Windows.Forms.Label
    Friend WithEvents lblSalesPersonName As System.Windows.Forms.Label
    Friend WithEvents lblSalesPerson As System.Windows.Forms.Label
    Friend WithEvents lblCommission As System.Windows.Forms.Label
    Friend WithEvents lblRetailPrice As System.Windows.Forms.Label
    Friend WithEvents lblCostPrice As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblStockNo As System.Windows.Forms.Label
    Friend WithEvents lblCommissionTxt As System.Windows.Forms.Label
    Friend WithEvents cboStockNumber As System.Windows.Forms.ComboBox
    Friend WithEvents txtRetailPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtCostPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents btnCommission As System.Windows.Forms.Button
    Friend WithEvents btnCalculate As System.Windows.Forms.Button

End Class
