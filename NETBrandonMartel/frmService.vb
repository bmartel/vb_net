﻿Public Class frmService

    Private Sub frmService_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    ''' <summary>
    ''' Check that there are values entered in all required text boxes, enable calculate context button when all fields
    ''' entered, disable when missing values
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles _
        txtServiceDesc.TextChanged, txtMaterials.TextChanged, txtParts.TextChanged, txtPrice.TextChanged, _
        txtServiceDesc.Leave, txtMaterials.Leave, txtParts.Leave, txtPrice.Leave

        Dim txtBox As TextBox = CType(sender, TextBox)

        If txtMaterials.Text <> "" And txtParts.Text <> "" And txtPrice.Text <> "" Then
            ctsmCalc.Enabled = True
        Else
            ctsmCalc.Enabled = False
        End If
        If txtBox.Text <> "" Then
            If IsNumeric(txtBox.Text) = False And txtBox.Name <> "txtServiceDesc" Then
                sendMessage("Data entered must be numeric.", "Data Error")
                ctsmCalc.Enabled = False

                txtBox.SelectAll()
                txtBox.Focus()
            End If
        End If
        
    End Sub
    ''' <summary>
    ''' select all text upon entering the field
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txt_TextEnter(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles _
        txtServiceDesc.Enter, txtMaterials.Enter, txtParts.Enter, txtPrice.Enter

        Dim txtBox As TextBox = CType(sender, TextBox)

        txtBox.SelectAll()
    End Sub

    ''' <summary>
    ''' calculate the total amount with pst and gst and display them in the labels of the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ctsmCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ctsmCalc.Click
        Dim newServiceCalc As BusinessTier.clsService = _
            New BusinessTier.clsService(Decimal.Parse(txtPrice.Text), Decimal.Parse(txtParts.Text), Decimal.Parse(txtMaterials.Text))
        lblTotalTxt.Text = FormatCurrency(newServiceCalc.calcAmount)
        lblPstTxt.Text = FormatCurrency(newServiceCalc.CalculatedPst)
        lblGstTxt.Text = FormatCurrency(newServiceCalc.CalculatedGst)
        ctsmSummary.Enabled = True
    End Sub

    ''' <summary>
    ''' opens the summary window
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ctsmSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ctsmSummary.Click
        Dim frmInstance As frmSummary = New frmSummary

        frmInstance.Show()
    End Sub

    ''' <summary>
    ''' clears the labels and textboxes, sets focus back to the ServiceDescription textbox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub ctsmClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ctsmClear.Click
        lblGstTxt.Text = ""
        lblPstTxt.Text = ""
        lblTotalTxt.Text = ""
        txtPrice.Text = ""
        txtParts.Text = ""
        txtMaterials.Text = ""
        txtServiceDesc.Text = ""
        txtServiceDesc.Focus()
    End Sub
End Class
