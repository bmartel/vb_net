﻿Public Class frmVBAuto
    Dim mobjData As New DataTier.clsData
    Dim mdsAllCars As New DataSet
    Dim mdsOneCar As New DataSet
    Dim mdsSalesStaff As New DataSet


    'ensureall bound controls, bound to the same dataset
    Dim mbmbSalesStaff As BindingManagerBase
    Dim mbmbAllCars As BindingManagerBase

    ''' <summary>
    ''' refresh data being retrieved and updated from the database
    ''' </summary>
    ''' <param name="strStockNo"></param>
    ''' <remarks></remarks>
    Private Sub refreshData(ByVal strStockNo As String)
        mdsAllCars.Clear()
        mdsAllCars = mobjData.getAllCars
        cboStockNumber.Text = strStockNo

    End Sub

    ''' <summary>
    ''' bind the corresponding datasets to the appropriate form controls
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub bindData()
        Try
            'bind sales staff data to the corresponding sales staff labels
            lblSalesPersonTxt.DataBindings.Add("text", mdsSalesStaff.Tables("SalesStaff"), "SalesID")
            lblSalesPersonNameTxt.DataBindings.Add("text", mdsSalesStaff.Tables("SalesStaff"), "SalesPersonName")
            lblDealerCodeTxt.DataBindings.Add("text", mdsSalesStaff.Tables("SalesStaff"), "DealerCode")

            'set binding context manager for sales staff
            mbmbSalesStaff = Me.BindingContext(mdsSalesStaff.Tables("SalesStaff"))


            'bind stock information data to the corresponding stock information labels
            With cboStockNumber
                .DataSource = mdsAllCars.Tables("UsedCars")
                .DisplayMember = "StockNo"
            End With
            txtDescription.DataBindings.Add("text", mdsAllCars.Tables("UsedCars"), "Description")
            txtCostPrice.DataBindings.Add("text", mdsAllCars.Tables("UsedCars"), "CostPrice")
            txtRetailPrice.DataBindings.Add("text", mdsAllCars.Tables("UsedCars"), "RetailPrice")

            'set binding context manager for stock information
            mbmbAllCars = Me.BindingContext(mdsAllCars.Tables("UsedCars"))
        Catch ex As Exception
            MessageBox.Show("Error in databinding" & ControlChars.NewLine & ex.Message, "Error Binding data")
        End Try

    End Sub

    ''' <summary>
    ''' onload connect to the database and retrieve all sales person info and all cars info from the database
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmVBAuto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mobjData.Connect()

            mdsSalesStaff = mobjData.getSalesStaff
            mdsAllCars = mobjData.getAllCars

            bindData()
        Catch ex As Exception
            sendMessage("There was an error " & ControlChars.NewLine & ex.Message, "Data Handling Exception")
        End Try
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        mbmbSalesStaff.Position += 1
        lblCommissionTxt.Text = ""
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        mbmbSalesStaff.Position -= 1
        lblCommissionTxt.Text = ""
    End Sub

    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        Try
            'declare a commission object and decimal representation to set cost and retail price
            Dim objCommission As BusinessTier.clsCommission = New BusinessTier.clsCommission(Decimal.Parse(txtCostPrice.Text), Decimal.Parse(txtRetailPrice.Text))
            Dim decCommission As Decimal = objCommission.calcCommission()

            lblCommissionTxt.Text = FormatCurrency(decCommission)

            'prepare the data adapter for the next update
            mobjData.getCommission()

            'update the commissions with the current parameters
            mobjData.updateCommission(lblSalesPersonNameTxt.Text, cboStockNumber.Text, txtDescription.Text, decCommission)
        Catch ex As Exception
            sendMessage("There was an error updatinng the commission " & ControlChars.NewLine & ex.Message, "Commission Error")
        End Try



    End Sub

    Private Sub btnCommission_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCommission.Click
        Dim frmCommInstance As frmCommission = New frmCommission()

        frmCommInstance.ShowDialog()
    End Sub

    Private Sub txtBoxShared_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescription.Leave, txtRetailPrice.Leave, txtCostPrice.Leave
        Dim txtBox As TextBox = CType(sender, TextBox)

        'check that text box has data and has been modified
        'if it is and is and contains data
        If (txtBox.Text.Length > 0 And txtBox.Modified) Then
            If (txtBox.Name = "txtCostPrice" Or txtBox.Name = "txtRetailPrice") Then
                If (IsNumeric(txtBox.Text)) Then
                Else
                    sendMessage("Value was must be numeric", "Value Not Numeric Error")
                    txtBox.Focus()
                    txtBox.SelectAll()
                End If
            End If
            'Create an instance of the Stock Maintenance form from existing Used Car Records
            Dim frmStockInstance As New frmStockMaintenance(cboStockNumber.Text, txtBox.Name, txtBox.Text)
            frmStockInstance.ShowDialog()

            'Call the refreshData() using the stockNo as the required argument
            refreshData(cboStockNumber.Text)
        End If
    End Sub

    Private Sub cboStockNumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStockNumber.Leave
        Dim drUserAnswer As DialogResult
        'check for numeric data with validateNumeric function
        If (validateNumeric(Trim(cboStockNumber.Text))) Then

            'Check if the Stock No already exists
            mdsOneCar = mobjData.getOneCar(Trim(cboStockNumber.Text))

            'Check if the dataset contains data
            If mdsOneCar.Tables("UsedCars").Rows.Count > 0 Then

                'Call our refreshData method to display the corresponding record associated with our StockNo
                refreshData(Trim(cboStockNumber.Text))
            Else

                'Check if the StockNo entered is less than or equal to 7 digits
                If Trim(cboStockNumber.Text.Length) <= 7 Then
                    'Prompt the user to create a new record
                    drUserAnswer = sendMessage("No value found in the database, would you like to add a new record to the database?", _
                                "Add a new record", _
                                MessageBoxButtons.YesNo)

                    'Check the users response to the messageBox
                    If drUserAnswer = Windows.Forms.DialogResult.Yes Then

                        'Create an instance of  the Stock Maintenance form
                        Dim frmStockInstance As New frmStockMaintenance(cboStockNumber.Text)
                        frmStockInstance.ShowDialog()

                        'Clear OneCar Dataset
                        mdsOneCar.Clear()

                        'retrieve the OneCar dataset corresponding to the currently selected stock number
                        mdsOneCar = mobjData.getOneCar(cboStockNumber.Text)

                        'If the OneCar DataSet contains data, then the user completed the insert operation successfully
                        If mdsOneCar.Tables("UsedCars").Rows.Count > 0 Then
                            refreshData(Trim(cboStockNumber.Text))
                        Else
                            'set the selectedIndex of the combobox to the first record
                            cboStockNumber.SelectedIndex = 0
                        End If
                    Else
                        cboStockNumber.Focus()

                    End If
                Else
                    'The StockNo was greater than 7 digits in length, send an error to user
                    sendMessage("StockNo cannot be greater than 7 digits", "StockNo Error")

                    'Set focus to our combobox and select all text
                    cboStockNumber.Focus()
                    cboStockNumber.SelectAll()

                End If


            End If
        Else
            cboStockNumber.Focus()
            cboStockNumber.SelectAll()
        End If
    End Sub

    ''' <summary>
    ''' Clear out the Commission label when stock number selection changes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cboStockNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStockNumber.SelectedIndexChanged
        lblCommissionTxt.Text = ""
    End Sub
End Class
