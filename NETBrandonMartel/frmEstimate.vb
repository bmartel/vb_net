﻿Public Class frmEstimate
    ''' <summary>
    ''' Constants for prices of car options
    ''' </summary>
    ''' <remarks></remarks>
    Const decSTEREO As Decimal = 505.05
    Const decLEATHER As Decimal = 1010.1
    Const decNAVIGATION As Decimal = 1515.15
    Const decSTANDARD As Decimal = 0.0
    Const decPEARLIZED As Decimal = 404.04
    Const decCUSTOM As Decimal = 606.06
    Const decTAXRATE As Decimal = 0.12D

    Dim mdecTotalAmountDue As Decimal
    Dim decAccessories As Decimal
    Dim decExteriorFinish As Decimal
    Dim mblnTest As Boolean = False
    Dim mdialogResult As DialogResult
    Dim mstrTestString As String

    'Accumulator variables for summary
    Dim mdecTotalEstimates As Decimal = 0.0
    Dim mdecTotalEstimateAmount As Decimal = 0.0


    ''' <summary>
    ''' Load Estimate Form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmEstimate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Show()
        quote(False)
        clear(False)
        enableFinance(False)
        tspmSummary.Enabled = False
        decAccessories = 0.0
        decExteriorFinish = 0.0
        txtCustomer.Focus()
    End Sub

    ''' <summary>
    ''' Change decExteriorFinish to the appropriate constant value for Exterior Finish, based on the selected radio button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub radStandard_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles radStandard.CheckedChanged, radCustom.CheckedChanged, radPearlized.CheckedChanged
        Dim radButton As RadioButton
        radButton = CType(sender, RadioButton)

        'Find which radio button is currently selected, assign appropriate constant 
        If radButton.Checked Then
            Select Case radButton.Name
                Case "radStandard"
                    decExteriorFinish = decSTANDARD
                Case "radPearlized"
                    decExteriorFinish = decPEARLIZED
                Case "radCustom"
                    decExteriorFinish = decCUSTOM
            End Select
        End If
    End Sub
    ''' <summary>
    ''' Calculate the amount of accessory options selected 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub chkStereoSystem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles chkStereoSystem.CheckedChanged, chkComputerNavigation.CheckedChanged, chkLeatherInterior.CheckedChanged
        Dim chkBox As CheckBox = CType(sender, CheckBox)

        'if a checkboxed was checked, find out which one and add the total of the corresponding accessory option constant
        'if a checkboxed fired the method and wasnt checked it was unchecked, therefore subtract the corresponding
        ' amount from the accessories total
        If chkBox.Checked Then
            Select Case chkBox.Name
                Case "chkStereoSystem"
                    decAccessories += decSTEREO
                Case "chkLeatherInterior"
                    decAccessories += decLEATHER
                Case "chkComputerNavigation"
                    decAccessories += decNAVIGATION
            End Select
        Else
            Select Case chkBox.Name
                Case "chkStereoSystem"
                    decAccessories -= decSTEREO
                Case "chkLeatherInterior"
                    decAccessories -= decLEATHER
                Case "chkComputerNavigation"
                    decAccessories -= decNAVIGATION
            End Select
        End If
        'MessageBox.Show("Accessories Total:" & decAccessories.ToString)
    End Sub

    ''' <summary>
    ''' Performs validation of textbox controls on enter
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtCustomer_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles txtCustomer.Enter, txtCarPrice.Enter, txtTradeInAllowance.Enter

        Dim txtBox As TextBox = CType(sender, TextBox)

        txtBox.SelectAll()

    End Sub

    ''' <summary>
    ''' performs validation of textbox controls on textchange/textchange
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles txtCarPrice.TextChanged, txtTradeInAllowance.TextChanged, txtCarPrice.Leave, txtTradeInAllowance.Leave

        Dim txtBox As TextBox = CType(sender, TextBox)

        'find out which txtbox triggered the event
        'test value of text field, if numeric enable calc button, if not disable the calc button
        'and set focus to the field in error

        Select Case txtBox.Name
            Case "txtTradeInAllowance"
                If NumCheck(txtBox) Then
                    clear(True)
                Else
                    clear(False)
                    enableFinance(False)
                    txtTradeInAllowance.Text = "0"
                    txtTradeInAllowance.Focus()
                    txtTradeInAllowance.SelectAll()
                End If
            Case "txtCarPrice"
                If NumCheck(txtBox) Then
                    clear(True)
                Else
                    clear(False)
                    enableFinance(False)
                    txtCarPrice.Text = "0"
                    txtCarPrice.Focus()
                    txtCarPrice.SelectAll()
                End If
        End Select
    End Sub

    ''' <summary>
    ''' evaluates for numeric data
    ''' </summary>
    ''' <param name="txtBox">textbox being evaluated</param>
    ''' <returns>true = data is numeric
    ''' false = data is not numeric or blank</returns>
    ''' <remarks></remarks>
    Private Function NumCheck(ByVal txtBox As TextBox) As Boolean
        Dim blnOK As Boolean

        If txtBox.Text.Length > 0 Then
            If IsNumeric(txtBox.Text) Then
                blnOK = True
            Else
                sendMessage("Data must be numeric", "Data error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                blnOK = False
            End If
        Else
            blnOK = False
        End If
        Return blnOK
    End Function

    ''' <summary>
    ''' Clear text areas and set focus to customer
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspmClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspmClear.Click
        txtTradeInAllowance.Text = ""
        txtCarPrice.Text = ""
        txtCustomer.Text = ""
        mlblAccessories.Text = ""
        mlblAmountDue.Text = ""
        mlblSalesTax.Text = ""
        mlblSubtotal.Text = ""
        mlblTotal.Text = ""
        chkStereoSystem.Checked = False
        chkLeatherInterior.Checked = False
        chkComputerNavigation.Checked = False
        radStandard.Checked = True

        txtCustomer.Focus()
        quote(False)
        clear(False)
        enableFinance(False)
    End Sub

    ''' <summary>
    ''' Close the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    ''' <summary>
    ''' Calculate sale
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspmCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspmCalculate.Click
        Dim decSalePrice As Decimal
        Dim decSubTotal As Decimal
        Dim decTotal As Decimal
        Dim decSalesTax As Decimal
        Dim decTradeIn As Decimal
        Dim decAmountDue As Decimal
        Try
            If txtCarPrice.Text.IndexOf("$") = 0 Then
                txtCarPrice.Text = txtCarPrice.Text.Remove(0, 0)
            End If
            If mlblSubtotal.Text.IndexOf("$") = 0 Then
                mlblSubtotal.Text = mlblSubtotal.Text.Remove(0, 0)
            End If
            If mlblSalesTax.Text.IndexOf("$") = 0 Then
                mlblSalesTax.Text = mlblSalesTax.Text.Remove(0, 0)
            End If
            If mlblAccessories.Text.IndexOf("$") = 0 Then
                mlblAccessories.Text = mlblAccessories.Text.Remove(0, 0)
            End If
            If txtTradeInAllowance.Text.IndexOf("$") = 0 Then
                txtTradeInAllowance.Text = txtTradeInAllowance.Text.Remove(0, 0)
            End If
            If mlblTotal.Text.IndexOf("$") = 0 Then
                mlblTotal.Text = mlblTotal.Text.Remove(0, 0)
            End If
            If mlblAmountDue.Text.IndexOf("$") = 0 Then
                mlblAmountDue.Text = mlblAmountDue.Text.Remove(0, 0)
            End If
                decSalePrice = CDec(txtCarPrice.Text)
                decTradeIn = CDec(txtTradeInAllowance.Text)
                decSubTotal = decSalePrice + decExteriorFinish + decAccessories
                decSalesTax = decSubTotal * decTAXRATE
                decTotal = decSubTotal + decSalesTax


                decAmountDue = decTotal - decTradeIn

                mdecTotalAmountDue = decAmountDue

                txtCarPrice.Text = FormatCurrency(decSalePrice, 2, 0, 0)
                mlblSubtotal.Text = FormatCurrency(decSubTotal, 2, 0, 0)
                mlblSalesTax.Text = FormatCurrency(decSalesTax, 2, 0, 0)
                mlblAccessories.Text = FormatCurrency((decExteriorFinish + decAccessories), 2, 0, 0)
                txtTradeInAllowance.Text = FormatCurrency(decTradeIn, 2, 0, 0)
                mlblTotal.Text = FormatCurrency(decTotal, 2, 0, 0)
                mlblAmountDue.Text = FormatCurrency(mdecTotalAmountDue, 2, 0, 0)


                txtCarPrice.Focus()
                txtCarPrice.SelectAll()

                quote(True)
                clear(True)
                enableFinance(True)
            calculate()
            If (mdecTotalEstimates < 1) Then
                tspmSummary.Enabled = False
            Else
                tspmSummary.Enabled = True
            End If
        Catch
            sendMessage("Bad Input data", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        

    End Sub
    ''' <summary>
    ''' allows user to suppress message box calls for testing purposes
    ''' </summary>
    ''' <param name="text"></param>
    ''' <param name="caption"></param>
    ''' <param name="buttons"></param>
    ''' <param name="icon"></param>
    ''' <param name="defaultButton"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function sendMessage(ByVal text As String, ByVal caption As String, Optional _
    ByVal buttons As MessageBoxButtons = MessageBoxButtons.OK, Optional ByVal icon As  _
    MessageBoxIcon = MessageBoxIcon.Information, Optional ByVal defaultButton As  _
    MessageBoxDefaultButton = MessageBoxDefaultButton.Button1) As DialogResult
        If mblnTest Then
            mstrTestString = text
            Return mdialogResult
        Else
            Return MessageBox.Show(text, caption, buttons, icon, defaultButton)
        End If
    End Function
    ''' <summary>
    ''' Changes label values when scroll bar is moved
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub hsbarNumYears_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hsbarNumYears.ValueChanged, hsbarInterestRate.ValueChanged
        Dim hsbr As HScrollBar = CType(sender, HScrollBar)

        'find which scrollbar is being accessed, change its matching label values accordingly
        Select Case hsbr.Name
            Case "hsbarNumYears"
                lblNumYearsTxt.Text = hsbr.Value.ToString()
            Case "hsbarInterestRate"
                lblInterestRateTxt.Text = FormatPercent((hsbr.Value / 10000.0), 2, TriState.True, TriState.False, TriState.UseDefault)
        End Select

        calculate()
    End Sub
    ''' <summary>
    ''' Enable/disable AcceptQuote menuitem based on whether the user passes true or false
    ''' </summary>
    ''' <param name="enabled"></param>
    ''' <remarks></remarks>
    Private Sub quote(ByVal enabled As Boolean)
        tspmAcceptQuote.Enabled = enabled
    End Sub

    ''' <summary>
    ''' Enable/disable Clear and Calculate menuitems based on whether the user passes true or false
    ''' </summary>
    ''' <param name="enabled"></param>
    ''' <remarks></remarks>
    Private Sub clear(ByVal enabled As Boolean)
        tspmClear.Enabled = enabled
        tspmCalculate.Enabled = enabled
    End Sub
    ''' <summary>
    ''' calculates the monthly payment based on the total amount due of the vehicle, interest rate and number of
    ''' payments selected
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub calculate()
        Dim interestRate As Double = (hsbarInterestRate.Value / 10000.0) / 12.0
        Dim numPayments As Double = hsbarNumYears.Value * 12.0
        Dim presentValue As Double = mdecTotalAmountDue
        Dim futureValue As Double = 0.0
        Dim monthlyPayment As Double = Math.Abs((Pmt(interestRate, numPayments, presentValue, futureValue, DueDate.EndOfPeriod)))

        lblMonthlyPaymentTxt.Text = FormatCurrency(monthlyPayment, 2, _
                                                TriState.UseDefault, TriState.False, TriState.UseDefault)

    End Sub
    ''' <summary>
    ''' Enables the finance group box, and sets the default of the scroll bar values 
    ''' </summary>
    ''' <param name="enabled"></param>
    ''' <remarks></remarks>
    Private Sub enableFinance(ByVal enabled As Boolean)
        grpFinance.Enabled = enabled

        If (enabled) Then
            If (lblNumYearsTxt.Text = "" Or lblNumYearsTxt.Text = "") Then
                hsbarNumYears.Value = 3
                hsbarInterestRate.Value = 500
            End If
            calculate()
        End If
    End Sub
    ''' <summary>
    ''' removes % sign from any string no matter where it is, I made this function because FormatNumber was not removing it,
    ''' and this was the quickest solution
    ''' </summary>
    ''' <param name="txt"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function RemovePercent(ByVal txt As String)
        Dim index As Integer = txt.IndexOf("%")
        If index >= 0 Then
            txt = txt.Remove(index)
        End If
        Return txt
    End Function

    ''' <summary>
    ''' 
    ''' accumulate quote estimates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspmAcceptQuote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspmAcceptQuote.Click

        mdecTotalEstimates += 1
        mdecTotalEstimateAmount += mdecTotalAmountDue
        tspmSummary.Enabled = True
        quote(False)

    End Sub
    ''' <summary>
    ''' Show summary of accumulated quote estimates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub tspmSummary_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tspmSummary.Click
        If (mdecTotalEstimates > 0) Then
            Dim decAvg As Decimal = mdecTotalEstimateAmount / mdecTotalEstimates
            Dim msg As String = "Total Estimates: " & FormatCurrency(mdecTotalEstimateAmount, 2) & vbNewLine & "Number of Estimates: " & FormatNumber(mdecTotalEstimates, 0) & vbNewLine & "Average: " & FormatCurrency(decAvg, 2)
            sendMessage(msg, "Summary of Accepted Quotes", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        
    End Sub
End Class