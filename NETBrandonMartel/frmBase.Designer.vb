﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.lblLine2 = New System.Windows.Forms.Label()
        Me.lblLine3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(135, 33)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(128, 20)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "VbAuto Centre"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(301, 281)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(75, 23)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblLine1.Location = New System.Drawing.Point(45, 261)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(330, 3)
        Me.lblLine1.TabIndex = 2
        '
        'lblLine2
        '
        Me.lblLine2.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblLine2.Location = New System.Drawing.Point(45, 281)
        Me.lblLine2.Name = "lblLine2"
        Me.lblLine2.Size = New System.Drawing.Size(250, 3)
        Me.lblLine2.TabIndex = 3
        '
        'lblLine3
        '
        Me.lblLine3.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblLine3.Location = New System.Drawing.Point(45, 301)
        Me.lblLine3.Name = "lblLine3"
        Me.lblLine3.Size = New System.Drawing.Size(250, 3)
        Me.lblLine3.TabIndex = 4
        Me.lblLine3.Text = "Label3"
        '
        'frmBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 336)
        Me.Controls.Add(Me.lblLine3)
        Me.Controls.Add(Me.lblLine2)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.lblTitle)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBase"
        Me.Text = "VbAuto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected WithEvents lblTitle As System.Windows.Forms.Label
    Protected WithEvents lblLine1 As System.Windows.Forms.Label
    Protected WithEvents lblLine3 As System.Windows.Forms.Label
    Protected WithEvents lblLine2 As System.Windows.Forms.Label
    Protected WithEvents btnExit As System.Windows.Forms.Button
End Class
