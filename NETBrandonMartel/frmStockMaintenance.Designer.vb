﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockMaintenance
    Inherits NETBrandonMartel.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblStockNo = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.txtCostPrice = New System.Windows.Forms.TextBox()
        Me.txtRetailPrice = New System.Windows.Forms.TextBox()
        Me.txtYear = New System.Windows.Forms.TextBox()
        Me.txtModel = New System.Windows.Forms.TextBox()
        Me.txtColour = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Location = New System.Drawing.Point(261, 33)
        '
        'lblLine1
        '
        Me.lblLine1.Location = New System.Drawing.Point(65, 364)
        Me.lblLine1.Size = New System.Drawing.Size(538, 3)
        '
        'lblLine3
        '
        Me.lblLine3.Location = New System.Drawing.Point(65, 424)
        Me.lblLine3.Size = New System.Drawing.Size(458, 3)
        '
        'lblLine2
        '
        Me.lblLine2.Location = New System.Drawing.Point(65, 404)
        Me.lblLine2.Size = New System.Drawing.Size(458, 3)
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(529, 404)
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(65, 384)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(458, 3)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Label1"
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(530, 375)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdate.TabIndex = 6
        Me.btnUpdate.Text = "&Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(68, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Stock #:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(68, 150)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(63, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Description:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(68, 216)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Cost Price:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(68, 283)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Retail Price:"
        '
        'lblStockNo
        '
        Me.lblStockNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStockNo.Location = New System.Drawing.Point(153, 86)
        Me.lblStockNo.Name = "lblStockNo"
        Me.lblStockNo.Size = New System.Drawing.Size(150, 20)
        Me.lblStockNo.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(350, 89)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Year:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(350, 150)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Model:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(349, 216)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(40, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Colour:"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(153, 147)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(151, 20)
        Me.txtDesc.TabIndex = 15
        '
        'txtCostPrice
        '
        Me.txtCostPrice.Location = New System.Drawing.Point(153, 213)
        Me.txtCostPrice.Name = "txtCostPrice"
        Me.txtCostPrice.Size = New System.Drawing.Size(151, 20)
        Me.txtCostPrice.TabIndex = 16
        '
        'txtRetailPrice
        '
        Me.txtRetailPrice.Location = New System.Drawing.Point(153, 280)
        Me.txtRetailPrice.Name = "txtRetailPrice"
        Me.txtRetailPrice.Size = New System.Drawing.Size(151, 20)
        Me.txtRetailPrice.TabIndex = 17
        '
        'txtYear
        '
        Me.txtYear.Location = New System.Drawing.Point(403, 86)
        Me.txtYear.Name = "txtYear"
        Me.txtYear.Size = New System.Drawing.Size(151, 20)
        Me.txtYear.TabIndex = 18
        '
        'txtModel
        '
        Me.txtModel.Location = New System.Drawing.Point(403, 147)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(151, 20)
        Me.txtModel.TabIndex = 19
        '
        'txtColour
        '
        Me.txtColour.Location = New System.Drawing.Point(403, 213)
        Me.txtColour.Name = "txtColour"
        Me.txtColour.Size = New System.Drawing.Size(151, 20)
        Me.txtColour.TabIndex = 20
        '
        'frmStockMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(662, 445)
        Me.Controls.Add(Me.txtColour)
        Me.Controls.Add(Me.txtModel)
        Me.Controls.Add(Me.txtYear)
        Me.Controls.Add(Me.txtRetailPrice)
        Me.Controls.Add(Me.txtCostPrice)
        Me.Controls.Add(Me.txtDesc)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblStockNo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnUpdate)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmStockMaintenance"
        Me.Text = "Stock Maintenance"
        Me.Controls.SetChildIndex(Me.lblTitle, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.lblLine1, 0)
        Me.Controls.SetChildIndex(Me.lblLine2, 0)
        Me.Controls.SetChildIndex(Me.lblLine3, 0)
        Me.Controls.SetChildIndex(Me.Label1, 0)
        Me.Controls.SetChildIndex(Me.btnUpdate, 0)
        Me.Controls.SetChildIndex(Me.Label2, 0)
        Me.Controls.SetChildIndex(Me.Label3, 0)
        Me.Controls.SetChildIndex(Me.Label4, 0)
        Me.Controls.SetChildIndex(Me.Label5, 0)
        Me.Controls.SetChildIndex(Me.lblStockNo, 0)
        Me.Controls.SetChildIndex(Me.Label7, 0)
        Me.Controls.SetChildIndex(Me.Label8, 0)
        Me.Controls.SetChildIndex(Me.Label9, 0)
        Me.Controls.SetChildIndex(Me.txtDesc, 0)
        Me.Controls.SetChildIndex(Me.txtCostPrice, 0)
        Me.Controls.SetChildIndex(Me.txtRetailPrice, 0)
        Me.Controls.SetChildIndex(Me.txtYear, 0)
        Me.Controls.SetChildIndex(Me.txtModel, 0)
        Me.Controls.SetChildIndex(Me.txtColour, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnUpdate As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblStockNo As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtCostPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtRetailPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtYear As System.Windows.Forms.TextBox
    Friend WithEvents txtModel As System.Windows.Forms.TextBox
    Friend WithEvents txtColour As System.Windows.Forms.TextBox

End Class
