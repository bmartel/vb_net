﻿Imports System.IO
Imports System.Reflection

Public Class frmCarWash

    ''' <summary>
    ''' structure to hold information of fragrances
    ''' </summary>
    ''' <remarks></remarks>
    Dim mfrgFragrances() As FragranceStruct

    ''' <summary>
    ''' arrays to store the information from the text files
    ''' </summary>
    ''' <remarks></remarks>
    Dim mstrInteriors() As String
    Dim mstrExteriors() As String
    Dim mdecPackagePrices() As Decimal
    Dim mstrFileNames() As String = {"Exterior.txt", "FragDesc.txt", "FragPrices.txt", "Interior.txt", "PkgDesc.txt", "PkgPrices.txt"}


    ''' <summary>
    ''' Structure to hold fragrances, with a price and description
    ''' </summary>
    ''' <remarks></remarks>
    Private Structure FragranceStruct
        Dim Description As String
        Dim Price As Decimal
    End Structure

    ''' <summary>
    ''' load carwash form, populate data into form from text files
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmCarWash_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        For Each strFileName As String In mstrFileNames
            readFiles(strFileName)
        Next

        'initialize comboboxes
        cboxDetailing.SelectedIndex = 0
        cboxFragrance.SelectedIndex = ResetFrag("pine")
    End Sub

    ''' <summary>
    ''' load values from text files into arrays/structures
    ''' </summary>
    ''' <param name="strFileName"></param>
    ''' <remarks></remarks>
    Private Sub readFiles(ByVal strFileName As String)
        Try
            Dim objAssembly As Assembly
            'populates an object with all assembly related data
            objAssembly = Assembly.GetExecutingAssembly()

            ' From the Assembly, the namespace can be obtained
            Dim strNamespace As String = objAssembly.GetName().Name.ToString()

            'use the namespace to locate the file within the project’s resources
            Dim srmFile As Stream

            srmFile = objAssembly.GetManifestResourceStream(strNamespace & "." & strFileName)
            Dim txtFile As New StreamReader(srmFile)
            Dim intIndex As Integer

            'loop through all the data line by line and populate the correct structure with the data
            Do While txtFile.Peek <> -1
                Select Case strFileName
                    Case "PkgDesc.txt"
                        cboxDetailing.Items.Add(txtFile.ReadLine())
                    Case "PkgPrices.txt"
                        ReDim Preserve mdecPackagePrices(intIndex)
                        mdecPackagePrices(intIndex) = Decimal.Parse(txtFile.ReadLine())
                    Case "FragDesc.txt"
                        ReDim Preserve mfrgFragrances(intIndex)
                        mfrgFragrances(intIndex).Description = txtFile.ReadLine()
                        cboxFragrance.Items.Add(mfrgFragrances(intIndex).Description)
                    Case "FragPrices.txt"
                        ReDim Preserve mfrgFragrances(intIndex)
                        mfrgFragrances(intIndex).Price = Decimal.Parse(txtFile.ReadLine())
                    Case "Interior.txt"
                        ReDim Preserve mstrInteriors(intIndex)
                        mstrInteriors(intIndex) = txtFile.ReadLine()
                    Case "Exterior.txt"
                        ReDim Preserve mstrExteriors(intIndex)
                        mstrExteriors(intIndex) = txtFile.ReadLine()
                End Select
                intIndex += 1
            Loop
            txtFile.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message & "\nError reading from file.")
            Me.Close()
        End Try
    End Sub

    ''' <summary>
    ''' Find the index of the name searched in the fragrance combobox
    ''' </summary>
    ''' <param name="strName"></param>
    ''' <returns>index if found, -1 if not found</returns>
    ''' <remarks></remarks>
    Private Function ResetFrag(ByVal strName As String) As Integer
        'parse out name into an array
        Dim strArray() As String = strName.Split(" ")
        Dim strFormattedString As String = ""

        'capitalize first letter of every word in the name array, make all other letters lowercase
        For Each strValue As String In strArray
            strFormattedString &= strValue.Substring(0, 1).ToUpper & strValue.Substring(1, strValue.Length - 1).ToLower & " "
        Next
        'return index match of the formatted name string
        Return cboxFragrance.FindString(strFormattedString.Trim)
    End Function

    ''' <summary>
    ''' Find the index of the name searched in the fragrance structure
    ''' </summary>
    ''' <param name="strName"></param>
    ''' <returns>index if found, -1 if not found</returns>
    ''' <remarks></remarks>
    Private Function GetFragIndex(ByVal strName As String) As Integer
        'parse out name into an array
        Dim strArray() As String = strName.Split(" ")
        Dim strFormattedString As String = ""
        Dim intIndex As Integer = -1
        Dim intCount As Integer = 0

        'capitalize first letter of every word in the name array, make all other letters lowercase
        For Each strValue As String In strArray
            strFormattedString &= strValue.Substring(0, 1).ToUpper & strValue.Substring(1, strValue.Length - 1).ToLower & " "
        Next

        'check for the index match of the formatted name string
        For Each strDesc In mfrgFragrances
            If strDesc.Description = strFormattedString.Trim Then
                intIndex = intCount
            End If
            intCount += 1
        Next

        Return intIndex
    End Function

    ''' <summary>
    ''' perform updates to data in the listboxes/comboboxes based on users selection choices in the comboboxes
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxDetailing.SelectedIndexChanged, cboxFragrance.SelectedIndexChanged
        Dim comboBox As ComboBox = CType(sender, ComboBox)

        Select Case comboBox.Name
            Case "cboxDetailing"
                cboxFragrance.SelectedIndex = ResetFrag("pine")
                lboxInterior.Items.Clear()
                lboxExterior.Items.Clear()

                Dim intCount As Integer = 0
                Dim intIndex As Integer = comboBox.SelectedIndex + 1
                While intCount < intIndex
                    lboxExterior.Items.Add(mstrExteriors(intCount))
                    lboxInterior.Items.Add(mstrInteriors(intCount))
                    intCount += 1
                End While

                'append fragrance as the first interior service description
                lboxInterior.Items(0) &= " - " & cboxFragrance.SelectedItem.ToString

            Case ("cboxFragrance")
                If lboxInterior.Items.Count > 0 Then
                    'append fragrance as the first interior service description
                    lboxInterior.Items(0) = "Fragrance - " & cboxFragrance.SelectedItem.ToString
                End If
        End Select

        'Create a new charges object to calculate the cost of the selected items
        Dim clsCost As BusinessTier.clsCharges = New BusinessTier.clsCharges(mdecPackagePrices(cboxDetailing.SelectedIndex), mfrgFragrances(cboxFragrance.SelectedIndex).Price)

        'Calulate cost of selected items
        clsCost.Calculate()

        'populate labels with the Get Properties procedures for charges, tax, and total
        lblChargesTxt.Text = FormatCurrency(clsCost.GetCharges)
        lblTaxesTxt.Text = FormatCurrency(clsCost.GetTax)
        lblTotalTxt.Text = FormatCurrency(clsCost.GetTotal)
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        'initialize comboboxes
        cboxDetailing.SelectedIndex = 0
        cboxFragrance.SelectedIndex = ResetFrag("pine")
    End Sub
End Class
