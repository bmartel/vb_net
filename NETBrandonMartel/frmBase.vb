﻿Public Class frmBase
    ''' <summary>
    ''' base form to be inherited from
    ''' </summary>

    Protected mblnTest As Boolean = False
    Protected mdialogResult As DialogResult
    Protected mstrTestString As String

    Private Sub frmBase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
    ''' <summary>
    ''' suppresses dialog boxes for test purposes
    ''' </summary>
    ''' <param name="text"></param>
    ''' <param name="caption"></param>
    ''' <param name="buttons"></param>
    ''' <param name="icon"></param>
    ''' <param name="defaultButton"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function sendMessage(ByVal text As String, ByVal caption As String, Optional _
        ByVal buttons As MessageBoxButtons = MessageBoxButtons.OK, Optional ByVal icon As  _
        MessageBoxIcon = MessageBoxIcon.Information, Optional ByVal defaultButton As  _
        MessageBoxDefaultButton = MessageBoxDefaultButton.Button1) As DialogResult

        If mblnTest = True Then
            mstrTestString = text
            Return mdialogResult
        Else
            Return MessageBox.Show(text, caption, buttons, icon, defaultButton)
        End If
    End Function

    ''' <summary>
    ''' checks if the value entered is numeric
    ''' </summary>
    ''' <param name="strValue"></param>
    ''' <returns>strIsValid</returns>
    ''' <remarks></remarks>
    Protected Function validateNumeric(ByVal strValue As String)
        Dim strIsValid As Boolean = False

        'check that the value is entered and numeric
        If strValue.Length > 0 Then
            If IsNumeric(strValue) Then
                strIsValid = True
            Else
                sendMessage("Data entered must be numeric", "Data Not Numeric Error")
            End If
        Else
            sendMessage("Data must be entered in this field", "Data Entry Error")
        End If

        Return strIsValid
    End Function
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub
End Class