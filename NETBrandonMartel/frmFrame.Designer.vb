﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFrame
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuMain = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOpen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEstimate = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuService = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCarWash = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCommission = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuWindow = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLayer = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCascade = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspbMain = New System.Windows.Forms.ToolStrip()
        Me.tspbOpen = New System.Windows.Forms.ToolStripDropDownButton()
        Me.tspmEstimate = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmService = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmCarWash = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmCommission = New System.Windows.Forms.ToolStripMenuItem()
        Me.tspmReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tspbTile = New System.Windows.Forms.ToolStripButton()
        Me.tspbLayer = New System.Windows.Forms.ToolStripButton()
        Me.tspbCascade = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tspbHelp = New System.Windows.Forms.ToolStripButton()
        Me.tspbExit = New System.Windows.Forms.ToolStripButton()
        Me.mnuMain.SuspendLayout()
        Me.tspbMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuMain
        '
        Me.mnuMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuWindow, Me.mnuHelp})
        Me.mnuMain.Location = New System.Drawing.Point(0, 0)
        Me.mnuMain.Name = "mnuMain"
        Me.mnuMain.Size = New System.Drawing.Size(284, 24)
        Me.mnuMain.TabIndex = 1
        Me.mnuMain.Text = "mnuMain"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOpen, Me.ToolStripMenuItem1, Me.mnuExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(37, 20)
        Me.mnuFile.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEstimate, Me.mnuService, Me.mnuCarWash, Me.mnuCommission, Me.mnuReports})
        Me.mnuOpen.Name = "mnuOpen"
        Me.mnuOpen.Size = New System.Drawing.Size(152, 22)
        Me.mnuOpen.Text = "&Open"
        '
        'mnuEstimate
        '
        Me.mnuEstimate.Name = "mnuEstimate"
        Me.mnuEstimate.Size = New System.Drawing.Size(152, 22)
        Me.mnuEstimate.Text = "&Estimate"
        '
        'mnuService
        '
        Me.mnuService.Name = "mnuService"
        Me.mnuService.Size = New System.Drawing.Size(152, 22)
        Me.mnuService.Text = "&Service"
        '
        'mnuCarWash
        '
        Me.mnuCarWash.Name = "mnuCarWash"
        Me.mnuCarWash.Size = New System.Drawing.Size(152, 22)
        Me.mnuCarWash.Text = "&Car Wash"
        '
        'mnuCommission
        '
        Me.mnuCommission.Name = "mnuCommission"
        Me.mnuCommission.Size = New System.Drawing.Size(152, 22)
        Me.mnuCommission.Text = "Co&mmision"
        '
        'mnuReports
        '
        Me.mnuReports.Name = "mnuReports"
        Me.mnuReports.Size = New System.Drawing.Size(152, 22)
        Me.mnuReports.Text = "&Reports"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(149, 6)
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        Me.mnuExit.Size = New System.Drawing.Size(152, 22)
        Me.mnuExit.Text = "E&xit"
        '
        'mnuWindow
        '
        Me.mnuWindow.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTile, Me.mnuLayer, Me.mnuCascade})
        Me.mnuWindow.Name = "mnuWindow"
        Me.mnuWindow.Size = New System.Drawing.Size(63, 20)
        Me.mnuWindow.Text = "&Window"
        '
        'mnuTile
        '
        Me.mnuTile.Name = "mnuTile"
        Me.mnuTile.Size = New System.Drawing.Size(118, 22)
        Me.mnuTile.Text = "Tile"
        '
        'mnuLayer
        '
        Me.mnuLayer.Name = "mnuLayer"
        Me.mnuLayer.Size = New System.Drawing.Size(118, 22)
        Me.mnuLayer.Text = "Layer"
        '
        'mnuCascade
        '
        Me.mnuCascade.Name = "mnuCascade"
        Me.mnuCascade.Size = New System.Drawing.Size(118, 22)
        Me.mnuCascade.Text = "Cascade"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
        Me.mnuHelp.Text = "&Help"
        '
        'mnuAbout
        '
        Me.mnuAbout.Name = "mnuAbout"
        Me.mnuAbout.Size = New System.Drawing.Size(107, 22)
        Me.mnuAbout.Text = "About"
        '
        'tspbMain
        '
        Me.tspbMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tspbOpen, Me.ToolStripSeparator1, Me.tspbTile, Me.tspbLayer, Me.tspbCascade, Me.ToolStripButton1, Me.tspbHelp, Me.tspbExit})
        Me.tspbMain.Location = New System.Drawing.Point(0, 24)
        Me.tspbMain.Name = "tspbMain"
        Me.tspbMain.Size = New System.Drawing.Size(284, 25)
        Me.tspbMain.TabIndex = 3
        Me.tspbMain.Text = "tspbMain"
        '
        'tspbOpen
        '
        Me.tspbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tspbOpen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tspmEstimate, Me.tspmService, Me.tspmCarWash, Me.tspmCommission, Me.tspmReports})
        Me.tspbOpen.Image = Global.NETBrandonMartel.My.Resources.Resources.open
        Me.tspbOpen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tspbOpen.Name = "tspbOpen"
        Me.tspbOpen.Size = New System.Drawing.Size(29, 22)
        Me.tspbOpen.Text = "Open"
        '
        'tspmEstimate
        '
        Me.tspmEstimate.Image = Global.NETBrandonMartel.My.Resources.Resources.sales
        Me.tspmEstimate.Name = "tspmEstimate"
        Me.tspmEstimate.Size = New System.Drawing.Size(141, 22)
        Me.tspmEstimate.Text = "Estimate"
        '
        'tspmService
        '
        Me.tspmService.Image = Global.NETBrandonMartel.My.Resources.Resources.service
        Me.tspmService.Name = "tspmService"
        Me.tspmService.Size = New System.Drawing.Size(141, 22)
        Me.tspmService.Text = "Service"
        '
        'tspmCarWash
        '
        Me.tspmCarWash.Image = Global.NETBrandonMartel.My.Resources.Resources.carwash
        Me.tspmCarWash.Name = "tspmCarWash"
        Me.tspmCarWash.Size = New System.Drawing.Size(141, 22)
        Me.tspmCarWash.Text = "Car Wash"
        '
        'tspmCommission
        '
        Me.tspmCommission.Image = Global.NETBrandonMartel.My.Resources.Resources.commission
        Me.tspmCommission.Name = "tspmCommission"
        Me.tspmCommission.Size = New System.Drawing.Size(141, 22)
        Me.tspmCommission.Text = "Commission"
        '
        'tspmReports
        '
        Me.tspmReports.Image = Global.NETBrandonMartel.My.Resources.Resources.reports
        Me.tspmReports.Name = "tspmReports"
        Me.tspmReports.Size = New System.Drawing.Size(141, 22)
        Me.tspmReports.Text = "Reports"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'tspbTile
        '
        Me.tspbTile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tspbTile.Image = Global.NETBrandonMartel.My.Resources.Resources.tile
        Me.tspbTile.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tspbTile.Name = "tspbTile"
        Me.tspbTile.Size = New System.Drawing.Size(23, 22)
        Me.tspbTile.Text = "Tile"
        '
        'tspbLayer
        '
        Me.tspbLayer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tspbLayer.Image = Global.NETBrandonMartel.My.Resources.Resources.layer
        Me.tspbLayer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tspbLayer.Name = "tspbLayer"
        Me.tspbLayer.Size = New System.Drawing.Size(23, 22)
        Me.tspbLayer.Text = "Layer"
        '
        'tspbCascade
        '
        Me.tspbCascade.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tspbCascade.Image = Global.NETBrandonMartel.My.Resources.Resources.cascade
        Me.tspbCascade.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tspbCascade.Name = "tspbCascade"
        Me.tspbCascade.Size = New System.Drawing.Size(23, 22)
        Me.tspbCascade.Text = "Cascade"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(6, 25)
        '
        'tspbHelp
        '
        Me.tspbHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tspbHelp.Image = Global.NETBrandonMartel.My.Resources.Resources.help
        Me.tspbHelp.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tspbHelp.Name = "tspbHelp"
        Me.tspbHelp.Size = New System.Drawing.Size(23, 22)
        Me.tspbHelp.Text = "Help"
        '
        'tspbExit
        '
        Me.tspbExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tspbExit.Image = Global.NETBrandonMartel.My.Resources.Resources._exit
        Me.tspbExit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tspbExit.Name = "tspbExit"
        Me.tspbExit.Size = New System.Drawing.Size(23, 22)
        Me.tspbExit.Text = "Exit"
        '
        'frmFrame
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.tspbMain)
        Me.Controls.Add(Me.mnuMain)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnuMain
        Me.Name = "frmFrame"
        Me.Text = "Auto Maintenance"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuMain.ResumeLayout(False)
        Me.mnuMain.PerformLayout()
        Me.tspbMain.ResumeLayout(False)
        Me.tspbMain.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuMain As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuWindow As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEstimate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuService As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCarWash As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCommission As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLayer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCascade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspbMain As System.Windows.Forms.ToolStrip
    Friend WithEvents tspbTile As System.Windows.Forms.ToolStripButton
    Friend WithEvents tspbLayer As System.Windows.Forms.ToolStripButton
    Friend WithEvents tspbCascade As System.Windows.Forms.ToolStripButton
    Friend WithEvents tspbExit As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tspbHelp As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tspbOpen As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents tspmEstimate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmService As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmCarWash As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmCommission As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tspmReports As System.Windows.Forms.ToolStripMenuItem

End Class
