﻿Public Class frmReport

    Private Sub frmReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objData As New DataTier.clsData
        Dim crRpt As New DataTier.crCommission() 'The report taht was created.
        Try
            objData.Connect()
            crRpt.SetDataSource(objData.getCommission)
            crvViewer.ReportSource = crRpt
        Catch Ex As Exception
            MessageBox.Show(Ex.Message, "Error")
        End Try
    End Sub
End Class
