﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSummary
    Inherits NETBrandonMartel.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTotalServiceContracts = New System.Windows.Forms.Label()
        Me.lblNumContracts = New System.Windows.Forms.Label()
        Me.lblTotalPst = New System.Windows.Forms.Label()
        Me.lblTotalGst = New System.Windows.Forms.Label()
        Me.lblAvgServiceContracts = New System.Windows.Forms.Label()
        Me.lblTotalServiceContractsTxt = New System.Windows.Forms.Label()
        Me.lblNumContractsTxt = New System.Windows.Forms.Label()
        Me.lblTotalPstTxt = New System.Windows.Forms.Label()
        Me.lblTotalGstTxt = New System.Windows.Forms.Label()
        Me.lblAvgServiceContractsTxt = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblTotalServiceContracts
        '
        Me.lblTotalServiceContracts.AutoSize = True
        Me.lblTotalServiceContracts.Location = New System.Drawing.Point(61, 94)
        Me.lblTotalServiceContracts.Name = "lblTotalServiceContracts"
        Me.lblTotalServiceContracts.Size = New System.Drawing.Size(121, 13)
        Me.lblTotalServiceContracts.TabIndex = 5
        Me.lblTotalServiceContracts.Text = "Total Service Contracts:"
        Me.lblTotalServiceContracts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumContracts
        '
        Me.lblNumContracts.AutoSize = True
        Me.lblNumContracts.Location = New System.Drawing.Point(36, 124)
        Me.lblNumContracts.Name = "lblNumContracts"
        Me.lblNumContracts.Size = New System.Drawing.Size(146, 13)
        Me.lblNumContracts.TabIndex = 6
        Me.lblNumContracts.Text = "Number of Service Contracts:"
        Me.lblNumContracts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalPst
        '
        Me.lblTotalPst.AutoSize = True
        Me.lblTotalPst.Location = New System.Drawing.Point(124, 154)
        Me.lblTotalPst.Name = "lblTotalPst"
        Me.lblTotalPst.Size = New System.Drawing.Size(58, 13)
        Me.lblTotalPst.TabIndex = 7
        Me.lblTotalPst.Text = "Total PST:"
        Me.lblTotalPst.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalGst
        '
        Me.lblTotalGst.AutoSize = True
        Me.lblTotalGst.Location = New System.Drawing.Point(123, 184)
        Me.lblTotalGst.Name = "lblTotalGst"
        Me.lblTotalGst.Size = New System.Drawing.Size(59, 13)
        Me.lblTotalGst.TabIndex = 8
        Me.lblTotalGst.Text = "Total GST:"
        Me.lblTotalGst.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAvgServiceContracts
        '
        Me.lblAvgServiceContracts.AutoSize = True
        Me.lblAvgServiceContracts.Location = New System.Drawing.Point(45, 214)
        Me.lblAvgServiceContracts.Name = "lblAvgServiceContracts"
        Me.lblAvgServiceContracts.Size = New System.Drawing.Size(137, 13)
        Me.lblAvgServiceContracts.TabIndex = 9
        Me.lblAvgServiceContracts.Text = "Average Service Contracts:"
        Me.lblAvgServiceContracts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalServiceContractsTxt
        '
        Me.lblTotalServiceContractsTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalServiceContractsTxt.Location = New System.Drawing.Point(199, 91)
        Me.lblTotalServiceContractsTxt.Name = "lblTotalServiceContractsTxt"
        Me.lblTotalServiceContractsTxt.Size = New System.Drawing.Size(119, 18)
        Me.lblTotalServiceContractsTxt.TabIndex = 10
        Me.lblTotalServiceContractsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNumContractsTxt
        '
        Me.lblNumContractsTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNumContractsTxt.Location = New System.Drawing.Point(199, 121)
        Me.lblNumContractsTxt.Name = "lblNumContractsTxt"
        Me.lblNumContractsTxt.Size = New System.Drawing.Size(119, 18)
        Me.lblNumContractsTxt.TabIndex = 11
        Me.lblNumContractsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalPstTxt
        '
        Me.lblTotalPstTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalPstTxt.Location = New System.Drawing.Point(199, 151)
        Me.lblTotalPstTxt.Name = "lblTotalPstTxt"
        Me.lblTotalPstTxt.Size = New System.Drawing.Size(119, 18)
        Me.lblTotalPstTxt.TabIndex = 12
        Me.lblTotalPstTxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalGstTxt
        '
        Me.lblTotalGstTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalGstTxt.Location = New System.Drawing.Point(199, 181)
        Me.lblTotalGstTxt.Name = "lblTotalGstTxt"
        Me.lblTotalGstTxt.Size = New System.Drawing.Size(119, 18)
        Me.lblTotalGstTxt.TabIndex = 13
        Me.lblTotalGstTxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAvgServiceContractsTxt
        '
        Me.lblAvgServiceContractsTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAvgServiceContractsTxt.Location = New System.Drawing.Point(199, 211)
        Me.lblAvgServiceContractsTxt.Name = "lblAvgServiceContractsTxt"
        Me.lblAvgServiceContractsTxt.Size = New System.Drawing.Size(119, 18)
        Me.lblAvgServiceContractsTxt.TabIndex = 14
        Me.lblAvgServiceContractsTxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(406, 336)
        Me.Controls.Add(Me.lblAvgServiceContractsTxt)
        Me.Controls.Add(Me.lblTotalGstTxt)
        Me.Controls.Add(Me.lblTotalPstTxt)
        Me.Controls.Add(Me.lblNumContractsTxt)
        Me.Controls.Add(Me.lblTotalServiceContractsTxt)
        Me.Controls.Add(Me.lblAvgServiceContracts)
        Me.Controls.Add(Me.lblTotalGst)
        Me.Controls.Add(Me.lblTotalPst)
        Me.Controls.Add(Me.lblNumContracts)
        Me.Controls.Add(Me.lblTotalServiceContracts)
        Me.Name = "frmSummary"
        Me.Text = "VbAuto Service Summary"
        Me.Controls.SetChildIndex(Me.lblTitle, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.lblLine1, 0)
        Me.Controls.SetChildIndex(Me.lblLine2, 0)
        Me.Controls.SetChildIndex(Me.lblLine3, 0)
        Me.Controls.SetChildIndex(Me.lblTotalServiceContracts, 0)
        Me.Controls.SetChildIndex(Me.lblNumContracts, 0)
        Me.Controls.SetChildIndex(Me.lblTotalPst, 0)
        Me.Controls.SetChildIndex(Me.lblTotalGst, 0)
        Me.Controls.SetChildIndex(Me.lblAvgServiceContracts, 0)
        Me.Controls.SetChildIndex(Me.lblTotalServiceContractsTxt, 0)
        Me.Controls.SetChildIndex(Me.lblNumContractsTxt, 0)
        Me.Controls.SetChildIndex(Me.lblTotalPstTxt, 0)
        Me.Controls.SetChildIndex(Me.lblTotalGstTxt, 0)
        Me.Controls.SetChildIndex(Me.lblAvgServiceContractsTxt, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTotalServiceContracts As System.Windows.Forms.Label
    Friend WithEvents lblNumContracts As System.Windows.Forms.Label
    Friend WithEvents lblTotalPst As System.Windows.Forms.Label
    Friend WithEvents lblTotalGst As System.Windows.Forms.Label
    Friend WithEvents lblAvgServiceContracts As System.Windows.Forms.Label
    Friend WithEvents lblTotalServiceContractsTxt As System.Windows.Forms.Label
    Friend WithEvents lblNumContractsTxt As System.Windows.Forms.Label
    Friend WithEvents lblTotalPstTxt As System.Windows.Forms.Label
    Friend WithEvents lblTotalGstTxt As System.Windows.Forms.Label
    Friend WithEvents lblAvgServiceContractsTxt As System.Windows.Forms.Label

End Class
