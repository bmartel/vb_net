﻿Public Class frmStockMaintenance
#Region "Module level Variables"
    'indicates if current record is: true = new, false = existing
    Dim mblnNewRecord As Boolean

    'indicates whether data has been entered in all controls on form
    Dim mblnContainsData As Boolean

    'modified values from commission form
    Dim mstrModifiedName As String
    Dim mstrModifiedText As String

    'modified stock number of new or existing record
    Dim mstrStockNo As String

    'datasets which will contain the data in question
    Dim mdsOneCar As DataSet
    Dim mdsAllCars As DataSet

    Dim mbmbOneCar As BindingManagerBase

    'clsData Object
    Dim mobjData As New DataTier.clsData

#End Region
    ''' <summary>
    ''' Bind our various controls to the form's One Car dataSet
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub bindData()
        'clear any previous databindings before setting new ones
        For Each ctrl As Control In Me.Controls
            ctrl.DataBindings.Clear()
        Next

        'Bind our Controls to the proper columns
        lblStockNo.DataBindings.Add("text",mdsOneCar.Tables("UsedCars"),"StockNo")
        txtDesc.DataBindings.Add("text",mdsOneCar.Tables("UsedCars"),"Description")
        txtCostPrice.DataBindings.Add("text",mdsOneCar.Tables("UsedCars"),"CostPrice")
        txtRetailPrice.DataBindings.Add("text",mdsOneCar.Tables("UsedCars"),"RetailPrice")
        txtYear.DataBindings.Add("text",mdsOneCar.Tables("UsedCars"),"ManufacturedYear")
        txtModel.DataBindings.Add("text",mdsOneCar.Tables("UsedCars"),"Model")
        txtColour.DataBindings.Add("text", mdsOneCar.Tables("UsedCars"), "Color")

        'Set the BindingManager to the form's BindingContext property
        mbmbOneCar = Me.BindingContext(mdsOneCar.Tables("UsedCars"))
    End Sub


    ''' <summary>
    ''' Parameterized constructor that accepts a new Stock No
    ''' </summary>
    ''' <param name="strStockNo">New Stock No</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal strStockNo As String)
        'Initialize all inherited objects (visual & non visual)
        InitializeComponent()

        'Set stockno label to the passed stockno
        lblStockNo.Text = strStockNo

        'flag as new record
        mblnNewRecord = True

        'Disable the update button
        btnUpdate.Enabled = False

    End Sub

    ''' <summary>
    ''' Parameterized constructor used when an existing record is found.
    ''' </summary>
    ''' <param name="strStockNo">StockNo</param>
    ''' <param name="strColumnName">Column Name changed</param>
    ''' <param name="strValue">Column value changed</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal strStockNo As String, ByVal strColumnName As String, ByVal strValue As String)
        'Initialize all inherited objects (visual & non visual)
        InitializeComponent()

        'Set stockNo label module level StockNo variable to the passed StockNo parameter
        lblStockNo.Text = strStockNo
        mstrStockNo = strStockNo

        'Set module level column name & column value variables to the passed parameters
        mstrModifiedName = strColumnName
        mstrModifiedText = strValue

        'flag as an existing record
        mblnNewRecord = False
    End Sub


    ''' <summary>
    ''' onload of form
    ''' connects to the database, determines if the record is new or existing and populates the form controls
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub frmStockMaintenance_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Try
            'to connect to database
            mobjData.Connect()

            'Check if the record is either a new record, or existing record using the module level boolean variable
            If mblnNewRecord Then
                'get all the records from the UsedCars Table and populate the AllCars DataSet
                mdsAllCars = mobjData.getAllCars

                'Change the text property of button to "Add"
                btnUpdate.Text = "Add"

                'Set focus to the description TextBox
                txtDesc.Focus()

            Else
                'get a single car record populating the dataset
                mdsOneCar = mobjData.getOneCar(mstrStockNo)

                'Bind controls
                bindData()

                'Determine which of the columns have changed using the name of the changed column to determine
                Select Case mstrModifiedName
                    Case txtDesc.Name
                        txtDesc.Text = mstrModifiedText
                    Case txtCostPrice.Name
                        txtCostPrice.Text = mstrModifiedText
                    Case txtRetailPrice.Name
                        txtRetailPrice.Text = mstrModifiedText
                    Case txtYear.Name
                        txtYear.Text = mstrModifiedText
                    Case txtModel.Name
                        txtModel.Text = mstrModifiedText
                    Case txtColour.Name
                        txtColour.Text = mstrModifiedText
                    Case Else
                        txtDesc.Focus()
                End Select


            End If
        Catch ex As Exception
            sendMessage(ex.Message, "Error")
        End Try
    End Sub

    ''' <summary>
    ''' Shared textbox event to handle when the user leaves a textbox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub txtShared_Leave(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles txtDesc.Leave, _
                txtCostPrice.Leave, _
                txtRetailPrice.Leave, _
                txtYear.Leave, _
                txtModel.Leave, _
                txtColour.Leave

        Dim txtBox As TextBox
        txtBox = CType(sender, TextBox)

        'Determine which textbox lost focus
        Select Case txtBox.Name
            Case txtDesc.Name
                If Trim(txtBox.Text).Length > 0 Or txtBox.Text = "" Then
                    mblnContainsData = True
                Else : mblnContainsData = False
                End If
            Case txtCostPrice.Name
                'Check if the value in the textbox is numeric
                If validateNumeric(txtBox.Text) Then
                    If Trim(txtBox.Text).Length > 0 Or txtBox.Text = "" Then
                        mblnContainsData = True
                    Else : mblnContainsData = False
                    End If
                Else
                    txtBox.SelectAll()
                    txtBox.Focus()
                End If

            Case txtRetailPrice.Name
                'Check if the value in the textbox is numeric
                If validateNumeric(txtBox.Text) Then
                    If Trim(txtBox.Text).Length > 0 Or txtBox.Text = "" Then
                        mblnContainsData = True
                    Else : mblnContainsData = False
                    End If
                Else
                    txtBox.SelectAll()
                    txtBox.Focus()
                End If

            Case txtYear.Name
                'Check if the value in the textbox is numeric and 4 digits long
                If validateNumeric(txtBox.Text) Then
                    If txtBox.Text.Length <> 4 Then
                        sendMessage("Year must be 4 digits long.", "Year Error")
                        txtBox.SelectAll()
                        txtBox.Focus()

                    End If
                Else
                    txtBox.SelectAll()
                    txtBox.Focus()
                End If

            Case txtModel.Name
                If Trim(txtBox.Text).Length > 0 Or txtBox.Text = "" Then
                    mblnContainsData = True
                Else : mblnContainsData = False
                End If

            Case txtColour.Name

                If Trim(txtBox.Text).Length > 0 Or txtBox.Text = "" Then
                    mblnContainsData = True
                Else : mblnContainsData = False
                End If

        End Select

        'Check if data is present in all text boxes
        If txtDesc.Text.Length > 0 And _
            txtCostPrice.Text.Length > 0 And _
            txtRetailPrice.Text.Length > 0 And _
            txtYear.Text.Length > 0 And _
            txtModel.Text.Length > 0 And _
            txtColour.Text.Length > 0 Then

            btnUpdate.Enabled = True
        Else
            btnUpdate.Enabled = False
        End If

    End Sub

    ''' <summary>
    ''' Used to update the UsedCars table in the VBAutoSales database with the current displayed information
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            'determine if this is a new record, or an existing record
            If mblnNewRecord Then
                mobjData.updateNewCar(lblStockNo.Text, _
                                      Integer.Parse(txtYear.Text), _
                                      txtDesc.Text, _
                                      txtModel.Text, txtColour.Text, _
                                      Decimal.Parse(txtCostPrice.Text), _
                                      Decimal.Parse(txtRetailPrice.Text))

                'Close the form
                Me.Close()
            Else
                'End the current edit
                mbmbOneCar.EndCurrentEdit()

                'Call the updateOneCar method and use the current dataset to update the database record
                mobjData.updateOneCar(mdsOneCar)

                'Close our form
                Me.Close()
            End If

        Catch ex As Exception
            sendMessage("Error modifying records", "Database Error")
        End Try

    End Sub
End Class
