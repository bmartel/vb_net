﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCarWash
    Inherits NETBrandonMartel.frmBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLine4 = New System.Windows.Forms.Label()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.cboxDetailing = New System.Windows.Forms.ComboBox()
        Me.cboxFragrance = New System.Windows.Forms.ComboBox()
        Me.lboxInterior = New System.Windows.Forms.ListBox()
        Me.lboxExterior = New System.Windows.Forms.ListBox()
        Me.lblDetailing = New System.Windows.Forms.Label()
        Me.lblFragrance = New System.Windows.Forms.Label()
        Me.lblInterior = New System.Windows.Forms.Label()
        Me.lblExterior = New System.Windows.Forms.Label()
        Me.lblCharges = New System.Windows.Forms.Label()
        Me.lblTaxes = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblChargesTxt = New System.Windows.Forms.Label()
        Me.lblTaxesTxt = New System.Windows.Forms.Label()
        Me.lblTotalTxt = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Location = New System.Drawing.Point(306, 26)
        '
        'lblLine1
        '
        Me.lblLine1.Location = New System.Drawing.Point(88, 418)
        Me.lblLine1.Size = New System.Drawing.Size(501, 3)
        '
        'lblLine3
        '
        Me.lblLine3.Location = New System.Drawing.Point(88, 450)
        Me.lblLine3.Size = New System.Drawing.Size(501, 3)
        '
        'lblLine2
        '
        Me.lblLine2.Location = New System.Drawing.Point(88, 434)
        Me.lblLine2.Size = New System.Drawing.Size(501, 3)
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(600, 433)
        '
        'lblLine4
        '
        Me.lblLine4.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblLine4.Location = New System.Drawing.Point(88, 401)
        Me.lblLine4.Name = "lblLine4"
        Me.lblLine4.Size = New System.Drawing.Size(585, 3)
        Me.lblLine4.TabIndex = 5
        Me.lblLine4.Text = "Label1"
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(600, 408)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 6
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'cboxDetailing
        '
        Me.cboxDetailing.FormattingEnabled = True
        Me.cboxDetailing.Location = New System.Drawing.Point(27, 105)
        Me.cboxDetailing.Name = "cboxDetailing"
        Me.cboxDetailing.Size = New System.Drawing.Size(121, 21)
        Me.cboxDetailing.TabIndex = 7
        '
        'cboxFragrance
        '
        Me.cboxFragrance.FormattingEnabled = True
        Me.cboxFragrance.Location = New System.Drawing.Point(27, 278)
        Me.cboxFragrance.Name = "cboxFragrance"
        Me.cboxFragrance.Size = New System.Drawing.Size(121, 21)
        Me.cboxFragrance.TabIndex = 8
        '
        'lboxInterior
        '
        Me.lboxInterior.FormattingEnabled = True
        Me.lboxInterior.Location = New System.Drawing.Point(310, 105)
        Me.lboxInterior.Name = "lboxInterior"
        Me.lboxInterior.Size = New System.Drawing.Size(158, 95)
        Me.lboxInterior.TabIndex = 9
        '
        'lboxExterior
        '
        Me.lboxExterior.FormattingEnabled = True
        Me.lboxExterior.Location = New System.Drawing.Point(515, 105)
        Me.lboxExterior.Name = "lboxExterior"
        Me.lboxExterior.Size = New System.Drawing.Size(158, 95)
        Me.lboxExterior.TabIndex = 10
        '
        'lblDetailing
        '
        Me.lblDetailing.AutoSize = True
        Me.lblDetailing.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetailing.Location = New System.Drawing.Point(27, 73)
        Me.lblDetailing.Name = "lblDetailing"
        Me.lblDetailing.Size = New System.Drawing.Size(57, 13)
        Me.lblDetailing.TabIndex = 11
        Me.lblDetailing.Text = "Detailing"
        '
        'lblFragrance
        '
        Me.lblFragrance.AutoSize = True
        Me.lblFragrance.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFragrance.Location = New System.Drawing.Point(27, 245)
        Me.lblFragrance.Name = "lblFragrance"
        Me.lblFragrance.Size = New System.Drawing.Size(64, 13)
        Me.lblFragrance.TabIndex = 12
        Me.lblFragrance.Text = "Fragrance"
        '
        'lblInterior
        '
        Me.lblInterior.AutoSize = True
        Me.lblInterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterior.Location = New System.Drawing.Point(361, 73)
        Me.lblInterior.Name = "lblInterior"
        Me.lblInterior.Size = New System.Drawing.Size(47, 13)
        Me.lblInterior.TabIndex = 13
        Me.lblInterior.Text = "Interior"
        '
        'lblExterior
        '
        Me.lblExterior.AutoSize = True
        Me.lblExterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExterior.Location = New System.Drawing.Point(572, 73)
        Me.lblExterior.Name = "lblExterior"
        Me.lblExterior.Size = New System.Drawing.Size(50, 13)
        Me.lblExterior.TabIndex = 14
        Me.lblExterior.Text = "Exterior"
        '
        'lblCharges
        '
        Me.lblCharges.AutoSize = True
        Me.lblCharges.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCharges.Location = New System.Drawing.Point(468, 244)
        Me.lblCharges.Name = "lblCharges"
        Me.lblCharges.Size = New System.Drawing.Size(57, 13)
        Me.lblCharges.TabIndex = 15
        Me.lblCharges.Text = "Charges:"
        '
        'lblTaxes
        '
        Me.lblTaxes.AutoSize = True
        Me.lblTaxes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTaxes.Location = New System.Drawing.Point(361, 295)
        Me.lblTaxes.Name = "lblTaxes"
        Me.lblTaxes.Size = New System.Drawing.Size(164, 13)
        Me.lblTaxes.TabIndex = 16
        Me.lblTaxes.Text = "Taxes (GST && PST) @ 12%:"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(485, 343)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(40, 13)
        Me.lblTotal.TabIndex = 17
        Me.lblTotal.Text = "Total:"
        '
        'lblChargesTxt
        '
        Me.lblChargesTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblChargesTxt.Location = New System.Drawing.Point(553, 245)
        Me.lblChargesTxt.Name = "lblChargesTxt"
        Me.lblChargesTxt.Size = New System.Drawing.Size(120, 23)
        Me.lblChargesTxt.TabIndex = 18
        Me.lblChargesTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTaxesTxt
        '
        Me.lblTaxesTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTaxesTxt.Location = New System.Drawing.Point(553, 294)
        Me.lblTaxesTxt.Name = "lblTaxesTxt"
        Me.lblTaxesTxt.Size = New System.Drawing.Size(120, 23)
        Me.lblTaxesTxt.TabIndex = 19
        Me.lblTaxesTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotalTxt
        '
        Me.lblTotalTxt.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalTxt.Location = New System.Drawing.Point(553, 342)
        Me.lblTotalTxt.Name = "lblTotalTxt"
        Me.lblTotalTxt.Size = New System.Drawing.Size(120, 23)
        Me.lblTotalTxt.TabIndex = 20
        Me.lblTotalTxt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmCarWash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(762, 496)
        Me.Controls.Add(Me.lblTotalTxt)
        Me.Controls.Add(Me.lblTaxesTxt)
        Me.Controls.Add(Me.lblChargesTxt)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblTaxes)
        Me.Controls.Add(Me.lblCharges)
        Me.Controls.Add(Me.lblExterior)
        Me.Controls.Add(Me.lblInterior)
        Me.Controls.Add(Me.lblFragrance)
        Me.Controls.Add(Me.lblDetailing)
        Me.Controls.Add(Me.lboxExterior)
        Me.Controls.Add(Me.lboxInterior)
        Me.Controls.Add(Me.cboxFragrance)
        Me.Controls.Add(Me.cboxDetailing)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.lblLine4)
        Me.Name = "frmCarWash"
        Me.Controls.SetChildIndex(Me.lblTitle, 0)
        Me.Controls.SetChildIndex(Me.btnExit, 0)
        Me.Controls.SetChildIndex(Me.lblLine1, 0)
        Me.Controls.SetChildIndex(Me.lblLine2, 0)
        Me.Controls.SetChildIndex(Me.lblLine3, 0)
        Me.Controls.SetChildIndex(Me.lblLine4, 0)
        Me.Controls.SetChildIndex(Me.btnClear, 0)
        Me.Controls.SetChildIndex(Me.cboxDetailing, 0)
        Me.Controls.SetChildIndex(Me.cboxFragrance, 0)
        Me.Controls.SetChildIndex(Me.lboxInterior, 0)
        Me.Controls.SetChildIndex(Me.lboxExterior, 0)
        Me.Controls.SetChildIndex(Me.lblDetailing, 0)
        Me.Controls.SetChildIndex(Me.lblFragrance, 0)
        Me.Controls.SetChildIndex(Me.lblInterior, 0)
        Me.Controls.SetChildIndex(Me.lblExterior, 0)
        Me.Controls.SetChildIndex(Me.lblCharges, 0)
        Me.Controls.SetChildIndex(Me.lblTaxes, 0)
        Me.Controls.SetChildIndex(Me.lblTotal, 0)
        Me.Controls.SetChildIndex(Me.lblChargesTxt, 0)
        Me.Controls.SetChildIndex(Me.lblTaxesTxt, 0)
        Me.Controls.SetChildIndex(Me.lblTotalTxt, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLine4 As System.Windows.Forms.Label
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents cboxDetailing As System.Windows.Forms.ComboBox
    Friend WithEvents cboxFragrance As System.Windows.Forms.ComboBox
    Friend WithEvents lboxInterior As System.Windows.Forms.ListBox
    Friend WithEvents lboxExterior As System.Windows.Forms.ListBox
    Friend WithEvents lblDetailing As System.Windows.Forms.Label
    Friend WithEvents lblFragrance As System.Windows.Forms.Label
    Friend WithEvents lblInterior As System.Windows.Forms.Label
    Friend WithEvents lblExterior As System.Windows.Forms.Label
    Friend WithEvents lblCharges As System.Windows.Forms.Label
    Friend WithEvents lblTaxes As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblChargesTxt As System.Windows.Forms.Label
    Friend WithEvents lblTaxesTxt As System.Windows.Forms.Label
    Friend WithEvents lblTotalTxt As System.Windows.Forms.Label

End Class
