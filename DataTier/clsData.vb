﻿Imports System.Data.OleDb

Public Class clsData
    ''' <summary>
    ''' Module level variables for db connection, data adapters, datasets, and commandbuilder objects
    ''' </summary>
    ''' <remarks></remarks>
    Dim mconAutoSales As OleDbConnection

    Dim mdaAllCars As New OleDbDataAdapter
    Dim mdaOneCar As New OleDbDataAdapter
    Dim mdaSalesStaff As New OleDbDataAdapter
    Dim mdaCommissions As New OleDbDataAdapter

    Dim mdsAllCars As New DataSet
    Dim mdsOneCar As New DataSet
    Dim mdsSalesStaff As New DataSet
    Dim mdsCommissions As New DataSet

    Dim mcmdAllCars As New OleDbCommandBuilder
    Dim mcmdOneCar As New OleDbCommandBuilder
    Dim mcmdCommissions As New OleDbCommandBuilder

    ''' <summary>
    ''' connect to the autosales database
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Connect()
        Try
            mconAutoSales = New OleDbConnection("provider = Microsoft.Jet.OLEDB.4.0; data source = VBAutoSales.mdb")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    ''' <summary>
    ''' retrieve the data set of all sales staff in the autosales db
    ''' </summary>
    ''' <returns>mdsSalesStaff</returns>
    ''' <remarks></remarks>
    Public Function getSalesStaff() As DataSet
        Try
            mdaSalesStaff.SelectCommand = New OleDbCommand
            mdaSalesStaff.SelectCommand.Connection = mconAutoSales
            mdaSalesStaff.SelectCommand.CommandText = "SELECT * FROM SalesStaff"

            mdaSalesStaff.Fill(mdsSalesStaff, "SalesStaff")

            Return mdsSalesStaff
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' retrieve the data set of all cars in the autosales db
    ''' </summary>
    ''' <returns>mdsAllCars</returns>
    ''' <remarks></remarks>
    Public Function getAllCars() As DataSet
        Try
            mdaAllCars.SelectCommand = New OleDbCommand
            mdaAllCars.SelectCommand.Connection = mconAutoSales
            mdaAllCars.SelectCommand.CommandText = "SELECT * FROM UsedCars"

            mdaAllCars.Fill(mdsAllCars, "UsedCars")

            'populate data adapter with update commands
            mcmdAllCars = New OleDbCommandBuilder(mdaAllCars)

            Return mdsAllCars
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' retrieve the data set of a specific car in the autosales db
    ''' </summary>
    ''' <param name="strStockNum"></param>
    ''' <returns>mdsOneCar</returns>
    ''' <remarks></remarks>
    Public Function getOneCar(ByVal strStockNum As String) As DataSet
        'Clear the dataset to make sure no data is overlapping causing an exception
        mdsOneCar.Clear()
        Try
            mdaOneCar.SelectCommand = New OleDbCommand
            mdaOneCar.SelectCommand.Connection = mconAutoSales
            mdaOneCar.SelectCommand.CommandText = "SELECT * FROM UsedCars WHERE StockNo = '" & strStockNum & "'"

            mdaOneCar.Fill(mdsOneCar, "UsedCars")

            'populate data adapter with update commands
            mcmdOneCar = New OleDbCommandBuilder(mdaOneCar)

            Return mdsOneCar
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' retrieve the data set of all sales commissions in the autosales db
    ''' </summary>
    ''' <returns>mdsCommissions</returns>
    ''' <remarks></remarks>
    Public Function getCommission() As DataSet
        Try
            mdaCommissions.SelectCommand = New OleDbCommand
            mdaCommissions.SelectCommand.Connection = mconAutoSales
            mdaCommissions.SelectCommand.CommandText = "SELECT * FROM Commissions"

            mdaCommissions.Fill(mdsCommissions, "Commissions")

            'populate data adapter with update commands
            mcmdCommissions = New OleDbCommandBuilder(mdaCommissions)

            Return mdsCommissions
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Insert a new commissions entry in the auto sales db
    ''' </summary>
    ''' <param name="strSalesPerson"></param>
    ''' <param name="strStockNumber"></param>
    ''' <param name="strVehicleDesc"></param>
    ''' <param name="decCommission"></param>
    ''' <remarks></remarks>
    Public Sub updateCommission(ByVal strSalesPerson As String, ByVal strStockNumber As String, ByVal strVehicleDesc As String, ByVal decCommission As Decimal)

        'instantiate a new row to insert commissions data
        Dim newCommission As DataRow

        'instantiate a new commission row
        newCommission = mdsCommissions.Tables("Commissions").NewRow
        newCommission("SalesPersonName") = strSalesPerson
        newCommission("StockNo") = strStockNumber
        newCommission("Description") = strVehicleDesc
        newCommission("Commission") = decCommission

        'Add new row to the dataset  
        mdsCommissions.Tables("Commissions").Rows.Add(newCommission)
        'make the above insert permanent with an update
        Try
            mdaCommissions.Update(mdsCommissions, "Commissions")
        Catch ex As Exception
            Throw
        End Try
    End Sub
    ''' <summary>
    ''' Insert a new car entry in the auto sales db
    ''' </summary>
    ''' <param name="strStockNumber"></param>
    ''' <param name="intManufacturedYear"></param>
    ''' <param name="strVehicleDesc"></param>
    ''' <param name="strModel"></param>
    ''' <param name="strColor"></param>
    ''' <param name="decCostPrice"></param>
    ''' <param name="decRetailPrice"></param>
    ''' <remarks></remarks>
    Public Sub updateNewCar(ByVal strStockNumber As String, ByVal intManufacturedYear As Integer, ByVal strVehicleDesc As String, ByVal strModel As String, ByVal strColor As String, ByVal decCostPrice As Decimal, ByVal decRetailPrice As Decimal)

        'instantiate a new row to insert commissions data
        Dim newCar As DataRow

        'instantiate a new commission row
        newCar = mdsAllCars.Tables("UsedCars").NewRow
        newCar("StockNo") = strStockNumber
        newCar("ManufacturedYear") = intManufacturedYear
        newCar("Description") = strVehicleDesc
        newCar("Model") = strModel
        newCar("Color") = strColor
        newCar("CostPrice") = decCostPrice
        newCar("RetailPrice") = decRetailPrice

        'Add new row to the dataset  
        mdsAllCars.Tables("UsedCars").Rows.Add(newCar)
        'make the above insert permanent with an update
        Try
            mdaAllCars.Update(mdsAllCars, "UsedCars")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub updateOneCar(ByVal dsCarChanges As DataSet)
        Try
            mdaOneCar.Update(dsCarChanges, "UsedCars")
        Catch ex As Exception
            Throw
        End Try
    End Sub


End Class

