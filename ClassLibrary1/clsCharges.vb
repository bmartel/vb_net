﻿Public Class clsCharges
    ''' <summary>
    ''' class constants
    ''' </summary>
    ''' <remarks></remarks>
    Private Const mdecTAXRATE As Decimal = 0.12D

    ''' <summary>
    ''' autoimplemented properties
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property PackagePrice As Decimal
    Property FragrancePrice As Decimal

    ''' <summary>
    ''' private class variables
    ''' </summary>
    ''' <remarks></remarks>
    Private mdecCharges As Decimal
    Private mdecTax As Decimal
    Private mdecTotal As Decimal


#Region "Readonly Properties"
    ''' <summary>
    ''' Gets the charges
    ''' </summary>
    ''' <value></value>
    ''' <returns>mdecCharges</returns>
    ''' <remarks></remarks>
    ReadOnly Property GetCharges
        Get
            Return mdecCharges
        End Get
    End Property
    ''' <summary>
    ''' Gets the tax
    ''' </summary>
    ''' <value></value>
    ''' <returns>mdecTax</returns>
    ''' <remarks></remarks>
    ReadOnly Property GetTax
        Get
            Return mdecTax
        End Get
    End Property
    ''' <summary>
    ''' Gets the total
    ''' </summary>
    ''' <value></value>
    ''' <returns>mdecTotal</returns>
    ''' <remarks></remarks>
    ReadOnly Property GetTotal
        Get
            Return mdecTotal
        End Get
    End Property
#End Region

    ''' <summary>
    ''' Default constructor
    ''' </summary>
    ''' <remarks>initializes all properties to 0</remarks>
    Public Sub New()
        mdecCharges = 0.0
        mdecTax = 0.0
        mdecTotal = 0.0

        PackagePrice = 0.0
        FragrancePrice = 0.0
    End Sub

    ''' <summary>
    ''' Parameterized constructor
    ''' </summary>
    ''' <param name="decPackagePrice"></param>
    ''' <param name="decFragrancePrice"></param>
    ''' <remarks>initializes the package price and fragrance price to passed values</remarks>
    Public Sub New(ByVal decPackagePrice As Decimal, ByVal decFragrancePrice As Decimal)
        mdecCharges = 0.0
        mdecTax = 0.0
        mdecTotal = 0.0

        PackagePrice = decPackagePrice
        FragrancePrice = decFragrancePrice
    End Sub

    ''' <summary>
    ''' Calculates total charges owing for purchases
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Calculate()
        mdecCharges = PackagePrice + FragrancePrice
        mdecTax = mdecCharges * mdecTAXRATE
        mdecTotal = mdecCharges + mdecTax
    End Sub
End Class
