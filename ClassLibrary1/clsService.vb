﻿Public Class clsService
    'class constants
    Protected Const mdecPSTRATE As Decimal = 0.07D
    Protected Const mdecGSTRATE As Decimal = 0.05D

    'auto-implemented properties
    Property Price As Decimal
    Property Parts As Decimal
    Property Materials As Decimal

    'class variables
    Private mdecCalculatedPst As Decimal
    Private mdecCalculatedGst As Decimal
    Private mdecCalculatedTotal As Decimal

    'shared properties
    Protected Shared mintNumContracts As Integer
    Protected Shared mdecAccumulatedCalculatedTotals As Decimal
    Protected Shared mdecAccumulatedGst As Decimal
    Protected Shared mdecAccumulatedPst As Decimal
    ''' <summary>
    ''' Readonly properties for class and shared properties/variables
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
#Region "Properties"
    ReadOnly Property CalculatedPst
        Get
            Return mdecCalculatedPst
        End Get
    End Property
    ReadOnly Property CalculatedGst
        Get
            Return mdecCalculatedGst
        End Get
    End Property
    ReadOnly Property CalculatedTotal
        Get
            Return mdecCalculatedTotal
        End Get
    End Property
    Shared ReadOnly Property NumberOfContracts
        Get
            Return mintNumContracts
        End Get
    End Property
    Shared ReadOnly Property AccumulatedTotals
        Get
            Return mdecAccumulatedCalculatedTotals
        End Get
    End Property
    Shared ReadOnly Property AccumulatedGst
        Get
            Return mdecAccumulatedGst
        End Get
    End Property
    Shared ReadOnly Property AccumulatedPst
        Get
            Return mdecAccumulatedPst
        End Get
    End Property
#End Region
    ''' <summary>
    ''' default constructor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        Price = 0
        Parts = 0
        Materials = 0
    End Sub
    ''' <summary>
    ''' parameterized constructor
    ''' </summary>
    ''' <param name="decPrice"></param>
    ''' <param name="decParts"></param>
    ''' <param name="decMaterials"></param>
    ''' <remarks></remarks>
    Public Sub New(ByVal decPrice As Decimal, ByVal decParts As Decimal, ByVal decMaterials As Decimal)
        Price = decPrice
        Parts = decParts
        Materials = decMaterials
    End Sub
    ''' <summary>
    ''' calculates service contract amount, accumulates totals, tax amounts and number of service contracts processed
    ''' </summary>
    ''' <returns>total service contract amount</returns>
    ''' <remarks></remarks>
    Public Function calcAmount()
        Dim decSubtotal As Decimal = Price + Parts + Materials
        mdecCalculatedGst = decSubtotal * mdecGSTRATE
        mdecCalculatedPst = decSubtotal * mdecPSTRATE
        mdecCalculatedTotal = decSubtotal + mdecCalculatedGst + mdecCalculatedPst

        mdecAccumulatedGst += mdecCalculatedGst
        mdecAccumulatedPst += mdecCalculatedPst
        mintNumContracts += 1
        mdecAccumulatedCalculatedTotals += mdecCalculatedTotal


        Return mdecCalculatedTotal
    End Function
    ''' <summary>
    ''' calculates the average of the service contracts
    ''' </summary>
    ''' <returns>The average amount of all service contracts processed</returns>
    ''' <remarks></remarks>
    Public Shared Function averageContract()
        Return mdecAccumulatedCalculatedTotals / mintNumContracts
    End Function

End Class
