﻿Public Class clsCommission
    ''' <summary>
    ''' Class constants
    ''' </summary>
    ''' <remarks></remarks>
    Protected Const mdecCOMMISSION_RATE As Decimal = 0.12D

    ''' <summary>
    ''' Auto implemented properties
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property CostPrice As Decimal
    Property RetailPrice As Decimal

    Public Sub New()
        CostPrice = 0.0
        RetailPrice = 0.0
    End Sub

    Public Sub New(ByVal decCost As Decimal, ByVal decRetail As Decimal)
        CostPrice = decCost
        RetailPrice = decRetail
    End Sub

    Public Function calcCommission()
        Return (RetailPrice - CostPrice) * mdecCOMMISSION_RATE
    End Function

End Class
