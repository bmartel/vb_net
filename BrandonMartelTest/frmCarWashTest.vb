﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports NETBrandonMartel



'''<summary>
'''This is a test class for frmCarWashTest and is intended
'''to contain all frmCarWashTest Unit Tests
'''</summary>
<TestClass()> _
Public Class frmCarWashTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for readFiles while reading in Exterior.txt
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub readFilesTest_Exterior()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strFileName As String = "Exterior.txt" ' TODO: Initialize to an appropriate value

        Dim expected As Integer = 4
        Dim actual As Integer

        target.readFiles(strFileName)

        actual = target.mstrExteriors.Length

        Assert.AreEqual(actual, expected, "Error: Values were inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for readFiles while reading in FragDesc.txt
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub readFilesTest_FragDesc()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strFileName As String = "FragDesc.txt" ' TODO: Initialize to an appropriate value

        Dim expected As Integer = 6
        Dim actual As Integer

        target.readFiles(strFileName)

        actual = target.mfrgFragrances.Length

        Assert.AreEqual(actual, expected, "Error: Values were inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for readFiles while reading in FragPrices.txt
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub readFilesTest_FragPrices()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strFileName As String = "FragPrices.txt" ' TODO: Initialize to an appropriate value

        Dim expected As Integer = 6
        Dim actual As Integer

        target.readFiles(strFileName)

        actual = target.mfrgFragrances.Length

        Assert.AreEqual(actual, expected, "Error: Values were inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for readFiles while reading in Interior.txt
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub readFilesTest_Interior()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strFileName As String = "Interior.txt" ' TODO: Initialize to an appropriate value

        Dim expected As Integer = 4
        Dim actual As Integer

        target.readFiles(strFileName)

        actual = target.mstrInteriors.Length

        Assert.AreEqual(actual, expected, "Error: Values were inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for readFiles while reading in PkgDesc.txt
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub readFilesTest_PkgDesc()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strFileName As String = "PkgDesc.txt" ' TODO: Initialize to an appropriate value

        Dim expected As Integer = 4
        Dim actual As Integer

        target.readFiles(strFileName)

        actual = target.cboxDetailing.Items.Count

        Assert.AreEqual(actual, expected, "Error: Values were inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for readFiles while reading in PkgPrices.txt
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub readFilesTest_PkgPrices()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strFileName As String = "PkgPrices.txt" ' TODO: Initialize to an appropriate value

        Dim expected As Integer = 4
        Dim actual As Integer

        target.readFiles(strFileName)

        actual = target.mdecPackagePrices.Length

        Assert.AreEqual(actual, expected, "Error: Values were inconsistent with expected")
    End Sub


    '''<summary>
    '''A test for ResetFrag
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub ResetFragTest()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strName As String = "Pine" ' TODO: Initialize to an appropriate value
        Dim expected As Integer = 2 ' TODO: Initialize to an appropriate value
        Dim actual As Integer

        target.readFiles("FragDesc.txt")
        actual = target.ResetFrag(strName)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for ResetFrag
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub ResetFragTest_Desc_with2Words()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strName As String = "Country Floral" ' TODO: Initialize to an appropriate value
        Dim expected As Integer = 3 ' TODO: Initialize to an appropriate value
        Dim actual As Integer

        target.readFiles("FragDesc.txt")
        actual = target.ResetFrag(strName)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for GetFragIndex
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub GetFragIndexTest()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strName As String = "Pine" ' TODO: Initialize to an appropriate value
        Dim expected As Integer = 2 ' TODO: Initialize to an appropriate value
        Dim actual As Integer

        target.readFiles("FragDesc.txt")
        actual = target.GetFragIndex(strName)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for GetFragIndex
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub GetFragIndexTest_with2words()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim strName As String = "Hawaiian Mist" ' TODO: Initialize to an appropriate value
        Dim expected As Integer = 0 ' TODO: Initialize to an appropriate value
        Dim actual As Integer

        target.readFiles("FragDesc.txt")
        actual = target.GetFragIndex(strName)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for frmCarWash_Load
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub frmCarWash_LoadTest()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value
        Dim expected1 As Integer = 0 ' TODO: Initialize to an appropriate value
        Dim actual1 As Integer
        Dim expected2 As Integer = 2 ' TODO: Initialize to an appropriate value
        Dim actual2 As Integer
        target.frmCarWash_Load(sender, e)

        actual1 = target.cboxDetailing.SelectedIndex
        actual2 = target.cboxFragrance.SelectedIndex

        Assert.AreEqual(expected1, actual1)
        Assert.AreEqual(expected2, actual2)
    End Sub

    '''<summary>
    '''A test for cbox_SelectedIndexChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub cbox_SelectedIndexChangedTest()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim cBoxSender As Object = target.cboxDetailing
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value
        Dim expected1 As Integer = 2
        Dim actual1 As Integer
        Dim expected2 As Integer = 1
        Dim actual2 As Integer

        target.frmCarWash_Load(sender, e)

        target.cboxDetailing.SelectedIndex = 1
        target.cboxFragrance.SelectedIndex = 1

        target.cbox_SelectedIndexChanged(cBoxSender, e)

        actual1 = target.cboxFragrance.SelectedIndex
        actual2 = target.cboxDetailing.SelectedIndex

        Assert.AreEqual(expected1, actual1)
        Assert.AreEqual(expected2, actual2)
    End Sub

    '''<summary>
    '''A test for btnClear_Click
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub btnClear_ClickTest()
        Dim target As frmCarWash_Accessor = New frmCarWash_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected1 As Integer = 0
        Dim actual1 As Integer
        Dim expected2 As Integer = 0
        Dim actual2 As Integer

        target.frmCarWash_Load(sender, e)

        target.cboxDetailing.SelectedIndex = 1
        target.cboxFragrance.SelectedIndex = 1


        target.btnClear_Click(sender, e)



        Assert.AreEqual(expected1, actual1)
        Assert.AreEqual(expected2, actual2)
    End Sub
End Class
