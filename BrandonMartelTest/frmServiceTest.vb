﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports NETBrandonMartel
Imports System.Windows.Forms



'''<summary>
'''This is a test class for frmServiceTest and is intended
'''to contain all frmServiceTest Unit Tests
'''</summary>
<TestClass()> _
Public Class frmServiceTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    Dim target As frmService_Accessor = New frmService_Accessor()
    <TestInitialize()> _
    Public Sub MyTestInitialize()
        target.mblnTest = True
    End Sub

    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for txt_TextChanged all fields entered
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txt_TextChangedTest()
        'Dim target As frmService_Accessor = New frmService_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtPrice ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        'setup
        target.txtPrice.Text = "12"
        target.txtParts.Text = "10"
        target.txtMaterials.Text = "5"
        Dim expected As Boolean = True
        Dim actual As Boolean


        target.txt_TextChanged(sender, e)
        actual = target.ctsmCalc.Enabled
        Assert.AreEqual(expected, actual, "Values inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for txt_TextChanged a field missing
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txtfieldmissing_TextChangedTest()
        'Dim target As frmService_Accessor = New frmService_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtPrice ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        'setup
        target.txtPrice.Text = "12"
        target.txtParts.Text = "10"
        Dim expected As Boolean = False
        Dim actual As Boolean


        target.txt_TextChanged(sender, e)
        actual = target.ctsmCalc.Enabled
        Assert.AreEqual(expected, actual, "Values inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for txt_TextChanged a field missing
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txtfielderror_TextChangedTest()
        'Dim target As frmService_Accessor = New frmService_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtMaterials ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        'setup
        target.txtPrice.Text = "12"
        target.txtParts.Text = "10"
        target.txtMaterials.Text = "text"

        Dim expectedError As String = "Data entered must be numeric."
        Dim actualError As String
        Dim expectedFocusString As String = "text"
        Dim actualFocusString As String
        Dim expected As Boolean = False
        Dim actual As Boolean


        target.txt_TextChanged(sender, e)
        actual = target.ctsmCalc.Enabled
        actualError = target.mstrTestString
        actualFocusString = target.txtMaterials.SelectedText

        Assert.AreEqual(expected, actual, "Values inconsistent with expected")
        Assert.AreEqual(expectedError, actualError, "Values inconsistent with expected")
        Assert.AreEqual(expectedFocusString, actualFocusString, "Values inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for txt_TextEnter
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txt_TextEnterTest()
        'Dim target As frmService_Accessor = New frmService_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtServiceDesc ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        'setup
        Dim expected As String = "Some Text"
        Dim actual As String

        target.txtServiceDesc.Text = "Some Text"

        target.txt_TextEnter(sender, e)
        actual = target.txtServiceDesc.SelectedText
        Assert.AreEqual(expected, actual, "Values inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for ctsmCalc_Click
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub ctsmCalc_ClickTest()
        'Dim target As frmService_Accessor = New frmService_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        target.txtMaterials.Text = "10"
        target.txtParts.Text = "10"
        target.txtPrice.Text = "10"

        'setup
        Dim expectedSummaryState As Boolean = True
        Dim actualSummaryState As Boolean

        Dim expectedTotal As String = "$33.60"
        Dim expectedGst As String = "$1.50"
        Dim expectedPst As String = "$2.10"
        Dim actualTotal As String
        Dim actualGst As String
        Dim actualPst As String

        target.ctsmCalc_Click(sender, e)
        actualSummaryState = target.ctsmSummary.Enabled
        actualTotal = target.lblTotalTxt.Text
        actualGst = target.lblGstTxt.Text
        actualPst = target.lblPstTxt.Text

        Assert.AreEqual(expectedSummaryState, actualSummaryState, "Values inconsistent with expected")
        Assert.AreEqual(expectedTotal, actualTotal, "Values inconsistent with expected")
        Assert.AreEqual(expectedGst, actualGst, "Values inconsistent with expected")
        Assert.AreEqual(expectedPst, actualPst, "Values inconsistent with expected")
    End Sub

    '''<summary>
    '''A test for ctsmClear_Click, check that calc button is disabled and that all text fields are empty, and focus is back at Service Description
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub ctsmClear_ClickTest()
        Dim target As frmService_Accessor = New frmService_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        'setup
        Dim expectedCalcState As Boolean = False
        Dim actualCalcState As Boolean

        Dim expectedPrice As String = ""
        Dim expectedParts As String = ""
        Dim expectedMaterials As String = ""
        Dim expectedServiceDescription As String = ""
        Dim expectedTotal As String = ""
        Dim expectedGst As String = ""
        Dim expectedPst As String = ""

        Dim actualPrice As String
        Dim actualParts As String
        Dim actualMaterials As String
        Dim actualServiceDescription As String
        Dim actualTotal As String
        Dim actualGst As String
        Dim actualPst As String

        target.ctsmClear_Click(sender, e)

        actualCalcState = target.ctsmCalc.Enabled
        actualServiceDescription = target.txtServiceDesc.SelectedText

        actualParts = target.txtParts.Text
        actualMaterials = target.txtMaterials.Text
        actualPrice = target.txtPrice.Text
        actualTotal = target.lblTotalTxt.Text
        actualGst = target.lblGstTxt.Text
        actualPst = target.lblPstTxt.Text

        Assert.AreEqual(expectedCalcState, actualCalcState, "Values inconsistent with expected")
        Assert.AreEqual(expectedParts, actualParts, "Values inconsistent with expected")
        Assert.AreEqual(expectedMaterials, actualMaterials, "Values inconsistent with expected")
        Assert.AreEqual(expectedPrice, actualPrice, "Values inconsistent with expected")
        Assert.AreEqual(expectedTotal, actualTotal, "Values inconsistent with expected")
        Assert.AreEqual(expectedGst, actualGst, "Values inconsistent with expected")
        Assert.AreEqual(expectedPst, actualPst, "Values inconsistent with expected")
        Assert.AreEqual(expectedServiceDescription, actualServiceDescription, "Values inconsistent with expected")
    End Sub
End Class
