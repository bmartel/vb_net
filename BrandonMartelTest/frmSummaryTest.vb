﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports NETBrandonMartel
Imports BusinessTier



'''<summary>
'''This is a test class for frmSummaryTest and is intended
'''to contain all frmSummaryTest Unit Tests
'''</summary>
<TestClass()> _
Public Class frmSummaryTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for frmSummary_Load with values
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub frmSummary_LoadTest()
        Dim target As frmSummary_Accessor = New frmSummary_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        'setup
        Dim expectedTotal As String = "$67.20"
        Dim expectedAvg As String = "$33.60"
        Dim expectedGst As String = "$3.00"
        Dim expectedPst As String = "$4.20"
        Dim expectedNumContracts As String = "2"

        Dim actualTotal As String
        Dim actualAvg As String
        Dim actualGst As String
        Dim actualPst As String
        Dim actualNumContracts As String

        BusinessTier.clsService_Accessor.mintNumContracts = 2
        BusinessTier.clsService_Accessor.mdecAccumulatedGst = 3.0
        BusinessTier.clsService_Accessor.mdecAccumulatedPst = 4.2
        BusinessTier.clsService_Accessor.mdecAccumulatedCalculatedTotals = 67.2


        'run form load
        target.frmSummary_Load(sender, e)

        'check values in labels
        actualAvg = target.lblAvgServiceContractsTxt.Text
        actualTotal = target.lblTotalServiceContractsTxt.Text
        actualGst = target._lblTotalGstTxt.Text
        actualPst = target.lblTotalPstTxt.Text
        actualNumContracts = target.lblNumContractsTxt.Text

        Assert.AreEqual(expectedAvg, actualAvg, "Values are inconsistent with expected")
        Assert.AreEqual(expectedTotal, actualTotal, "Values are inconsistent with expected")
        Assert.AreEqual(expectedGst, actualGst, "Values are inconsistent with expected")
        Assert.AreEqual(expectedPst, actualPst, "Values are inconsistent with expected")
        Assert.AreEqual(expectedNumContracts, actualNumContracts, "Values are inconsistent with expected")
    End Sub

End Class
