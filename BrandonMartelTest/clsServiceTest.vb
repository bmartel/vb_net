﻿Imports System.Windows.Forms

Imports NETBrandonMartel

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports BusinessTier



'''<summary>
'''This is a test class for clsServiceTest and is intended
'''to contain all clsServiceTest Unit Tests
'''</summary>
<TestClass()> _
Public Class clsServiceTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test

    Dim target As BusinessTier.clsService_Accessor = New BusinessTier.clsService_Accessor()

    <TestInitialize()> _
    Public Sub MyTestInitialize()
        BusinessTier.clsService_Accessor.mintNumContracts = 0
        BusinessTier.clsService_Accessor.mdecAccumulatedCalculatedTotals = 0
        BusinessTier.clsService_Accessor.mdecAccumulatedGst = 0
        BusinessTier.clsService_Accessor.mdecAccumulatedPst = 0

    End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for Price
    '''</summary>
    <TestMethod()> _
    Public Sub PriceTest()
        'Dim target As clsService = New clsService() ' TODO: Initialize to an appropriate value
        Dim expected As [Decimal] = 2 ' TODO: Initialize to an appropriate value
        Dim actual As [Decimal]
        target.Price = expected
        actual = target.Price
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Parts
    '''</summary>
    <TestMethod()> _
    Public Sub PartsTest()
        'Dim target As clsService = New clsService() ' TODO: Initialize to an appropriate value
        Dim expected As [Decimal] = 2 ' TODO: Initialize to an appropriate value
        Dim actual As [Decimal]
        target.Parts = expected
        actual = target.Parts
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for Materials
    '''</summary>
    <TestMethod()> _
    Public Sub MaterialsTest()
        'Dim target As clsService = New clsService() ' TODO: Initialize to an appropriate value
        Dim expected As [Decimal] = 2 ' TODO: Initialize to an appropriate value
        Dim actual As [Decimal]
        target.Materials = expected
        actual = target.Materials
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for clsService Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub clsServiceConstructorTest()
        Dim decPrice As [Decimal] = 12.0 ' TODO: Initialize to an appropriate value
        Dim decParts As [Decimal] = 12.0 ' TODO: Initialize to an appropriate value
        Dim decMaterials As [Decimal] = 12.0 ' TODO: Initialize to an appropriate value

        Dim actualPrice As Decimal
        Dim actualParts As Decimal
        Dim actualMaterials As Decimal
        Dim expectedPrice As Decimal = 12.0
        Dim expectedParts As Decimal = 12.0
        Dim expectedMaterials As Decimal = 12.0

        Dim target As clsService = New clsService(decPrice, decParts, decMaterials)
        actualMaterials = target.Materials
        actualParts = target.Parts
        actualPrice = target.Price

        Assert.AreEqual(expectedPrice, actualPrice, "Values are inconsistent with expected")
        Assert.AreEqual(expectedParts, actualParts, "Values are inconsistent with expected")
        Assert.AreEqual(expectedMaterials, actualMaterials, "Values are inconsistent with expected")


        expectedPrice = 0
        expectedParts = 0
        expectedMaterials = 0

        Dim targetDefault As clsService = New clsService()
        actualMaterials = targetDefault.Materials
        actualParts = targetDefault.Parts
        actualPrice = targetDefault.Price

        Assert.AreEqual(expectedPrice, actualPrice, "Values are inconsistent with expected")
        Assert.AreEqual(expectedParts, actualParts, "Values are inconsistent with expected")
        Assert.AreEqual(expectedMaterials, actualMaterials, "Values are inconsistent with expected")

    End Sub

    '''<summary>
    '''A test for calcAmount
    '''</summary>
    <TestMethod()> _
    Public Sub calcAmountTest()
        Dim target As clsService = New clsService() ' TODO: Initialize to an appropriate value
        Dim expectedServiceAmount As Decimal = 33.6
        Dim actualServiceAmount As Decimal
        Dim expectedNumContracts As Integer = 1
        Dim actualNumContracts As Integer
        Dim expectedAccumulatedServiceAmount As Decimal = 33.6
        Dim actualAccumulatedServiceAmount As Decimal
        Dim expectedAccumulatedGst As Decimal = 1.5
        Dim actualAccumulatedGst As Decimal
        Dim expectedAccumulatedPst As Decimal = 2.1
        Dim actualAccumulatedPst As Decimal

        'setup service contract #1
        target.Price = 10
        target.Parts = 10
        target.Materials = 10

        actualServiceAmount = target.calcAmount
        actualNumContracts = clsService.NumberOfContracts
        actualAccumulatedServiceAmount = clsService.AccumulatedTotals
        actualAccumulatedGst = clsService.AccumulatedGst
        actualAccumulatedPst = clsService.AccumulatedPst

        Assert.AreEqual(expectedServiceAmount, actualServiceAmount)
        Assert.AreEqual(expectedNumContracts, actualNumContracts)
        Assert.AreEqual(expectedAccumulatedServiceAmount, actualAccumulatedServiceAmount)
        Assert.AreEqual(expectedAccumulatedGst, actualAccumulatedGst)
        Assert.AreEqual(expectedAccumulatedPst, actualAccumulatedPst)

        'setup service contract #2
        expectedNumContracts = 2
        expectedAccumulatedServiceAmount = 67.2
        expectedAccumulatedGst = 3.0
        expectedAccumulatedPst = 4.2

        target.Price = 10
        target.Parts = 10
        target.Materials = 10

        actualServiceAmount = target.calcAmount
        actualNumContracts = clsService.NumberOfContracts
        actualAccumulatedServiceAmount = clsService.AccumulatedTotals
        actualAccumulatedGst = clsService.AccumulatedGst
        actualAccumulatedPst = clsService.AccumulatedPst

        Assert.AreEqual(expectedServiceAmount, actualServiceAmount)
        Assert.AreEqual(expectedNumContracts, actualNumContracts)
        Assert.AreEqual(expectedAccumulatedServiceAmount, actualAccumulatedServiceAmount)
        Assert.AreEqual(expectedAccumulatedGst, actualAccumulatedGst)
        Assert.AreEqual(expectedAccumulatedPst, actualAccumulatedPst)
    End Sub

    '''<summary>
    '''A test for averageContract
    '''</summary>
    <TestMethod()> _
    Public Sub averageContractTest()
        Dim expected As Decimal = 33.6 ' TODO: Initialize to an appropriate value
        Dim actual As Decimal

        target.Price = 10
        target.Parts = 10
        target.Materials = 10

        target.calcAmount()
        target.Price = 10
        target.Parts = 10
        target.Materials = 10

        target.calcAmount()

        actual = clsService.averageContract
        Assert.AreEqual(expected, actual)
    End Sub
End Class
