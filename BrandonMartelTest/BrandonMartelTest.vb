﻿Imports BusinessTier

Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports NETBrandonMartel
Imports System.Windows.Forms



'''<summary>
'''This is a test class for frmEstimateTest and is intended
'''to contain all frmEstimateTest Unit Tests
'''</summary>
<TestClass()> _
Public Class frmEstimateTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for frmEstimate_Load
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub frmEstimate_LoadTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value
        target.frmEstimate_Load(sender, e)
    End Sub

    '''<summary>
    '''A test for radStandard_CheckedChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub radStandard_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.radStandard ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 0.0
        Dim actual As Decimal

        target.radStandard.Checked = True
        target.radStandard_CheckedChanged(sender, e)

        actual = target.decExteriorFinish
        Assert.AreEqual(expected, actual, "Error: The constant value supplied is different than value expected!")
    End Sub

    '''<summary>
    '''A test for radStandard_CheckedChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub radPearlized_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.radPearlized ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 404.04
        Dim actual As Decimal

        target.radPearlized.Checked = True
        target.radStandard_CheckedChanged(sender, e)


        actual = target.decExteriorFinish
        Assert.AreEqual(expected, actual, "Error: The constant value supplied is different than value expected!")
    End Sub

    '''<summary>
    '''A test for radCustom_CheckedChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub radCustom_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.radCustom ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 606.06
        Dim actual As Decimal

        target.radCustom.Checked = True

        target.radStandard_CheckedChanged(sender, e)

        actual = target.decExteriorFinish

        Assert.AreEqual(expected, actual, "Error: The constant value supplied is different than value expected!")
    End Sub

    '''<summary>
    '''A test for chkStereo_CheckedChanged None checked
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkNone_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 0.0
        Dim actual As Decimal

        'if none are selected
        target.chkComputerNavigation.Checked = False
        target.chkStereoSystem.Checked = False
        target.chkLeatherInterior.Checked = False

        'get the value of selected options(should be 0)
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub

    '''<summary>
    '''A test for chkStereo_CheckedChanged All checked
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkAll_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 3030.3
        Dim actual As Decimal

        'if all are selected
        target.chkComputerNavigation.Checked = True
        target.chkStereoSystem.Checked = True
        target.chkLeatherInterior.Checked = True

        'get the value of selected options
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub

    '''<summary>
    '''A test for chkStereo_CheckedChanged leather/nav
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkLeatherNav_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 2525.25
        Dim actual As Decimal

        'if all are selected
        target.chkComputerNavigation.Checked = True
        target.chkLeatherInterior.Checked = True

        'get the value of selected options
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub

    '''<summary>
    '''A test for chkStereo_CheckedChanged leather/stereo
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkLeatherStereo_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 1515.15
        Dim actual As Decimal

        'if leather and stereo are selected
        target.chkStereoSystem.Checked = True
        target.chkLeatherInterior.Checked = True

        'get the value of selected options
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub

    '''<summary>
    '''A test for chkStereo_CheckedChanged Nav/Stereo
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkNavStereo_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 2020.2
        Dim actual As Decimal

        'if all are selected
        target.chkComputerNavigation.Checked = True
        target.chkStereoSystem.Checked = True

        'get the value of selected options
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub

    '''<summary>
    '''A test for chkStereoSystem_CheckedChanged stereo only
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkStereoSystem_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 505.05
        Dim actual As Decimal

        'if stereo is checked by itself
        target.chkStereoSystem.Checked = True

        'get the value of just the stereo
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub
    '''<summary>
    '''A test for chkStereoSystem_CheckedChanged leather only
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkLeatherInterior_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 1010.1
        Dim actual As Decimal

        'if leather interior is checked by itself
        target.chkLeatherInterior.Checked = True

        'get the value of just the leather interior
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub
    '''<summary>
    '''A test for chkComputerNavigation_CheckedChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub chkComputerNavigation_CheckedChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.chkStereoSystem ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As Decimal = 1515.15
        Dim actual As Decimal

        'if computer navigation is checked by itself
        target.chkComputerNavigation.Checked = True

        'get the value of just the computer navigation
        actual = target.decAccessories
        Assert.AreEqual(expected, actual, "Error: values are not consistent with what is expected")
    End Sub
    '''<summary>
    '''A test for txtCustomer_Enter
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txtCustomer_EnterTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtCustomer ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "This is a test"
        Dim actual As String
        'set text of field
        target.txtCustomer.Text = "This is a test"

        target.txtCustomer_Enter(sender, e)
        'check on enter if the text is selected
        actual = target.txtCustomer.SelectedText
        Assert.AreEqual(expected, actual, "Error: Values are inconsistent.")
    End Sub


    '''<summary>
    '''A test for txtTradeInAllowance_Enter
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txtTradeInAllowance_EnterTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtTradeInAllowance ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value
        target.mblnTest = True
        Dim expected As String = "0"
        Dim actual As String
        'set text of field
        target.txtTradeInAllowance.Text = "This is a test"

        target.txtCustomer_Enter(sender, e)
        'check on enter if the text is selected
        actual = target.txtTradeInAllowance.SelectedText
        Assert.AreEqual(expected, actual, "Error: Values are inconsistent.")
    End Sub

    '''<summary>
    '''A test for txtCarPrice_Enter
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txtCarPrice_EnterTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtCarPrice ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "100"
        Dim actual As String
        'set text of field
        target.txtCarPrice.Text = "100"

        target.txtCustomer_Enter(sender, e)
        'check on enter if the text is selected
        actual = target.txtCarPrice.SelectedText
        Assert.AreEqual(expected, actual, "Error: Values are inconsistent.")
    End Sub

    '''<summary>
    '''A test for txt_TextChanged isNumeric
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txt_TextChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtCarPrice ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value


        Dim expected As Boolean = True
        Dim expected2 As Boolean = True
        Dim actual As Boolean
        Dim actual2 As Boolean

        target.mblnTest = True

        target.txtCarPrice.Text = "10000"
        target._txtTradeInAllowance.Text = "1500"

        target.txt_TextChanged(sender, e)

        actual = target.tspmCalculate.Enabled
        actual2 = target.tspmClear.Enabled
        Assert.AreEqual(expected, actual, "Error values inconsistent")
        Assert.AreEqual(expected2, actual2, "Error values inconsistent")
    End Sub

    '''<summary>
    '''A test for txt_TextChanged nonNumeric
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub txtstring_TextChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.txtTradeInAllowance ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value


        Dim expected As Boolean = True
        Dim expected2 As Boolean = True
        Dim actual As Boolean
        Dim actual2 As Boolean

        target.mblnTest = True

        target.txtTradeInAllowance.Text = "asdaasasd"
        target.txtCarPrice.Text = "dndfsdnldn"
        target.txt_TextChanged(sender, e)

        actual = target.tspmCalculate.Enabled
        actual2 = target.tspmClear.Enabled
        Assert.AreEqual(expected, actual, "Error values inconsistent")
        Assert.AreEqual(expected2, actual2, "Error values inconsistent")
    End Sub

    '''<summary>
    '''A test for btnCalc_Click Sale price, trade in and accessory option and finish selected
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub btnCalc_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim enabled As Boolean = True
        Dim isEnabled As Boolean

        Dim expected As String = "$11,218.18"
        Dim actual As String

        target.txtCarPrice.Text = "10000"
        target.txtTradeInAllowance.Text = "1000"
        target.chkStereoSystem.Checked = True
        target.radPearlized.Checked = True

        target.tspmCalc_Click(sender, e)
        actual = target.mlblAmountDue.Text

        isEnabled = target.tspmAcceptQuote.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: Inconsistent values.")

        isEnabled = target.tspmClear.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: Inconsistent values.")

        isEnabled = target.grpFinance.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: Inconsistent values.")

        Assert.AreEqual(expected, actual, "Error: Inconsistent values.")
    End Sub

    '''<summary>
    '''A test for btnCalc_Click Sale price, accessory option and finish selected
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmCalcNoTrade_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "$12,218.18"
        Dim actual As String

        target.txtCarPrice.Text = "10000"
        target.txtTradeInAllowance.Text = "0"
        target.chkStereoSystem.Checked = True
        target.radPearlized.Checked = True

        target.tspmCalc_Click(sender, e)
        actual = target.mlblAmountDue.Text
        Assert.AreEqual(expected, actual, "Error: Inconsistent values.")
    End Sub
    '''<summary>
    '''A test for btnCalc_Click Sale price, Trade in and finish selected
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmCalcNoAcc_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "$10,652.52"
        Dim actual As String

        target.txtCarPrice.Text = "10000"
        target.txtTradeInAllowance.Text = "1000"
        target.radPearlized.Checked = True

        target.tspmCalc_Click(sender, e)
        actual = target.mlblAmountDue.Text
        Assert.AreEqual(expected, actual, "Error: Inconsistent values.")
    End Sub
    '''<summary>
    '''A test for btnCalc_Click Sale price, Trade in accessories and finish selected
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmCalcHighTrade_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "-$3,381.82"
        Dim actual As String

        target.txtCarPrice.Text = "5000"
        target.txtTradeInAllowance.Text = "10000"
        target.radPearlized.Checked = True
        target.chkStereoSystem.Checked = True

        target.tspmCalc_Click(sender, e)
        actual = target.mlblAmountDue.Text
        Assert.AreEqual(expected, actual, "Error: Inconsistent values.")
    End Sub
    '''<summary>
    '''A test for btnCalc_Click Sale price, Trade in accessories and finish selected 2 calcs in a row
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmCalcx2_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected1 As String = "-$3,381.82"
        Dim actual1 As String

        Dim expected2 As String = "-$3,381.82"
        Dim actual2 As String

        target.txtCarPrice.Text = "5000"
        target.txtTradeInAllowance.Text = "10000"
        target.radPearlized.Checked = True
        target.chkStereoSystem.Checked = True

        target.tspmCalc_Click(sender, e)
        actual1 = target.mlblAmountDue.Text

        target.tspmCalc_Click(sender, e)
        actual2 = target.mlblAmountDue.Text

        Assert.AreEqual(expected1, actual1, "Error: Inconsistent values.")
        Assert.AreEqual(expected2, actual2, "Error: Inconsistent values.")
    End Sub

    '''<summary>
    '''A test for tspmClear_Click
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmClear_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim enabled As Boolean = False
        Dim isEnabled As Boolean
        Dim expected As String = ""
        Dim actual As String

        target.tspmClear_Click(sender, e)

        actual = target.txtTradeInAllowance.Text &
                    target.txtCarPrice.Text &
                    target.txtCustomer.Text &
                    target.mlblAccessories.Text &
                    target.mlblAmountDue.Text &
                    target.mlblSalesTax.Text &
                    target.mlblSubtotal.Text &
                    target.mlblTotal.Text

        isEnabled = target.tspmAcceptQuote.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: Inconsistent values.")

        isEnabled = target.tspmClear.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: Inconsistent values.")

        isEnabled = target.grpFinance.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: Inconsistent values.")
        Assert.AreEqual(expected, actual, "Error: Inconsistent values.")
    End Sub
    '''<summary>
    '''A test for quote, disabling of menu item Accept Quote
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub quoteTest_false()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = False ' TODO: Initialize to an appropriate value

        Dim expected As Boolean = False
        Dim actual As Boolean

        target.quote(enabled)

        actual = target.tspmAcceptQuote.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for quote, enabling of menu item Accept Quote
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub quoteTest_true()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = True ' TODO: Initialize to an appropriate value

        Dim expected As Boolean = True
        Dim actual As Boolean

        target.quote(enabled)

        actual = target.tspmAcceptQuote.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub

    '''<summary>
    '''A test for clear
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub clearTest_false()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = False ' TODO: Initialize to an appropriate value

        Dim expected As Boolean = False
        Dim actual As Boolean

        target.clear(enabled)

        actual = target.tspmClear.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")

        actual = target.tspmCalculate.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for clear
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub clearTest_true()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = True ' TODO: Initialize to an appropriate value

        Dim expected As Boolean = True
        Dim actual As Boolean

        target.clear(enabled)

        actual = target.tspmClear.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")

        actual = target.tspmCalculate.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub

    '''<summary>
    '''A test for calulate with max interest
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub calculateTest_maxInterest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value


        Dim expected As String = "$597.76"
        Dim actual As String

        target.mdecTotalAmountDue = 11200.0
        target.hsbarInterestRate.Value = 2500
        target.hsbarNumYears.Value = 2



        target.calculate()

        actual = target.lblMonthlyPaymentTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for calulate with min interest
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub calculateTest_minInterest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value


        Dim expected As String = "$466.67"
        Dim actual As String

        target.mdecTotalAmountDue = 11200.0
        target.hsbarInterestRate.Value = 0
        target.hsbarNumYears.Value = 2



        target.calculate()

        actual = target.lblMonthlyPaymentTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for calulate with max finance years
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub calculateTest_maxYears()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value


        Dim expected As String = "$254.79"
        Dim actual As String

        target.mdecTotalAmountDue = 11200.0
        target.hsbarInterestRate.Value = 2500
        target.hsbarNumYears.Value = 10



        target.calculate()

        actual = target.lblMonthlyPaymentTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for calulate with min finance years
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub calculateTest_minYears()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value


        Dim expected As String = "$1,064.50"
        Dim actual As String

        target.mdecTotalAmountDue = 11200.0
        target.hsbarInterestRate.Value = 2500
        target.hsbarNumYears.Value = 1



        target.calculate()

        actual = target.lblMonthlyPaymentTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for calulate with no amount due
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub calculateTest_noAmountDue()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value


        Dim expected As String = "$0.00"
        Dim actual As String

        target.mdecTotalAmountDue = 0.0
        target.hsbarInterestRate.Value = 2500
        target.hsbarNumYears.Value = 2



        target.calculate()

        actual = target.lblMonthlyPaymentTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub

    '''<summary>
    '''A test for enableFinance with true, finance paramaters changed
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub enableFinanceTest_true()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = True ' TODO: Initialize to an appropriate value
        Dim expectedInterest As String = "2.50%"
        Dim expectedYears As String = "10"

        Dim actualInterest As String
        Dim actualYears As String

        target.hsbarInterestRate.Value = 250
        target.hsbarNumYears.Value = 10

        target.enableFinance(enabled)

        actualInterest = target.lblInterestRateTxt.Text
        actualYears = target.lblNumYearsTxt.Text
        Assert.AreEqual(expectedInterest, actualInterest, "Error: values are inconsistent with what is expected")
        Assert.AreEqual(expectedYears, actualYears, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for enableFinance with true, finance paramaters changed
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub enableFinanceTest_true_params()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = True ' TODO: Initialize to an appropriate value

        Dim expectedYears As String = "3"
        Dim expectedInterest As String = "5.00%"
        Dim actualYears As String
        Dim actualInterest As String

        target.enableFinance(enabled)
        actualYears = target.lblNumYearsTxt.Text
        actualInterest = target.lblInterestRateTxt.Text

        Assert.AreEqual(expectedYears, actualYears, "Error: values are inconsistent with what is expected")
        Assert.AreEqual(expectedInterest, actualInterest, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for enableFinance with false
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub enableFinanceTest_false()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim enabled As Boolean = False ' TODO: Initialize to an appropriate value

        Dim yearsLabel As String = ""
        Dim expected As Boolean = False
        Dim actual As Boolean
        Dim actualYears As String

        target.enableFinance(enabled)
        actualYears = target.lblNumYearsTxt.Text
        actual = target.grpFinance.Enabled
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
        Assert.AreEqual(yearsLabel, actualYears, "Error: values are inconsistent with what is expected")
    End Sub

    '''<summary>
    '''A test for hsbarNumYears_ValueChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub hsbarNumYears_ValueChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.hsbarNumYears ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "5"
        Dim actual As String

        target.hsbarNumYears.Value = 5
        target.hsbarNumYears_ValueChanged(sender, e)

        actual = target.lblNumYearsTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub
    '''<summary>
    '''A test for hsbarNumYears_ValueChanged
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub hsbarInterestRate_ValueChangedTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = target.hsbarInterestRate ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim expected As String = "5.00%"
        Dim actual As String

        target.hsbarInterestRate.Value = 500
        target.hsbarNumYears_ValueChanged(sender, e)

        actual = target.lblInterestRateTxt.Text
        Assert.AreEqual(expected, actual, "Error: values are inconsistent with what is expected")
    End Sub

    '''<summary>
    '''A test for tspmAcceptQuote_Click
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmAcceptQuote_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        Dim enabled As Boolean = True
        Dim notEnabled As Boolean = False
        Dim isEnabled As Boolean

        Dim expectedAmount As Decimal = 10000.0
        Dim expectedTotal As Decimal = 1.0
        Dim accumAmount As Decimal
        Dim accumTotal As Decimal

        target.mdecTotalEstimates = 0
        target.mdecTotalEstimateAmount = 0

        target.mdecTotalAmountDue = 10000.0

        target.tspmAcceptQuote_Click(sender, e)

        accumAmount = target.mdecTotalEstimateAmount
        accumTotal = target.mdecTotalEstimates

        Assert.AreEqual(expectedAmount, accumAmount, "Error: values are inconsistent with what is expected")
        Assert.AreEqual(expectedTotal, accumTotal, "Error: values are inconsistent with what is expected")

        isEnabled = target.tspmSummary.Enabled
        Assert.AreEqual(enabled, isEnabled, "Error: values are inconsistent with what is expected")

        isEnabled = target.tspmAcceptQuote.Enabled
        Assert.AreEqual(notEnabled, isEnabled, "Error: values are inconsistent with what is expected")
    End Sub

    '''<summary>
    '''A test for tspmSummary_Click
    '''</summary>
    <TestMethod(), _
     DeploymentItem("NETBrandonMartel.exe")> _
    Public Sub tspmSummary_ClickTest()
        Dim target As frmEstimate_Accessor = New frmEstimate_Accessor() ' TODO: Initialize to an appropriate value
        Dim sender As Object = Nothing ' TODO: Initialize to an appropriate value
        Dim e As EventArgs = Nothing ' TODO: Initialize to an appropriate value

        target.mblnTest = True
        Dim expectedBeforeQuote As Boolean = False
        Dim expectedAfterQuote As Boolean = True
        Dim actual As Boolean

        target.txtCarPrice.Text = "10000"
        target.txtTradeInAllowance.Text = "1000"
        target.chkStereoSystem.Checked = True
        target.radPearlized.Checked = True

        target.tspmCalc_Click(sender, e)

        actual = target.tspmSummary.Enabled
        Assert.AreEqual(expectedBeforeQuote, actual, "Error: values are inconsistent with what is expected")

        target.tspmAcceptQuote_Click(sender, e)


        target.tspmSummary_Click(sender, e)

        actual = target.tspmSummary.Enabled
        Assert.AreEqual(expectedAfterQuote, actual, "Error: values are inconsistent with what is expected")
    End Sub
End Class
