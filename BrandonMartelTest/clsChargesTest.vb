﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports BusinessTier



'''<summary>
'''This is a test class for clsChargesTest and is intended
'''to contain all clsChargesTest Unit Tests
'''</summary>
<TestClass()> _
Public Class clsChargesTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region


    '''<summary>
    '''A test for GetCharges
    '''</summary>
    <TestMethod()> _
    Public Sub GetChargesTest()
        Dim target As clsCharges_Accessor = New clsCharges_Accessor() ' TODO: Initialize to an appropriate value
        target.mdecCharges = 10.0
        Dim expected As Decimal = 10.0
        Dim actual As Decimal
        actual = target.GetCharges
        Assert.AreEqual(actual, expected, "Error: inconsistent values with what is to be expected")
    End Sub

    '''<summary>
    '''A test for GetTax
    '''</summary>
    <TestMethod()> _
    Public Sub GetTaxTest()
        Dim target As clsCharges_Accessor = New clsCharges_Accessor() ' TODO: Initialize to an appropriate value
        target.mdecTax = 10.0
        Dim expected As Decimal = 10.0
        Dim actual As Decimal
        actual = target.GetTax
        Assert.AreEqual(actual, expected, "Error: inconsistent values with what is to be expected")
    End Sub

    '''<summary>
    '''A test for GetTotal
    '''</summary>
    <TestMethod()> _
    Public Sub GetTotalTest()
        Dim target As clsCharges_Accessor = New clsCharges_Accessor() ' TODO: Initialize to an appropriate value
        target.mdecTotal = 10.0
        Dim expected As Decimal = 10.0
        Dim actual As Decimal
        actual = target.GetTotal
        Assert.AreEqual(actual, expected, "Error: inconsistent values with what is to be expected")
    End Sub

    '''<summary>
    '''A test for clsCharges Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub clsChargesConstructorTest()
        Dim decPackagePrice As [Decimal] = 10.0 ' TODO: Initialize to an appropriate value
        Dim decFragrancePrice As [Decimal] = 10.0 ' TODO: Initialize to an appropriate value
        Dim target As clsCharges = New clsCharges(decPackagePrice, decFragrancePrice)

        Dim actual1 As Decimal
        Dim actual2 As Decimal
        Dim expected1 As Decimal = 10.0
        Dim expected2 As Decimal = 10.0

        actual1 = target.PackagePrice
        actual2 = target.FragrancePrice
        Assert.AreEqual(actual1, expected1, "Error: inconsistent values with what is to be expected")
        Assert.AreEqual(actual2, expected2, "Error: inconsistent values with what is to be expected")
    End Sub

    '''<summary>
    '''A test for clsCharges Constructor
    '''</summary>
    <TestMethod()> _
    Public Sub clsChargesConstructorTest1()
        Dim target As clsCharges = New clsCharges()

        Dim actual1 As Decimal
        Dim actual2 As Decimal
        Dim expected1 As Decimal = 0.0
        Dim expected2 As Decimal = 0.0

        actual1 = target.PackagePrice
        actual2 = target.FragrancePrice
        Assert.AreEqual(actual1, expected1, "Error: inconsistent values with what is to be expected")
        Assert.AreEqual(actual2, expected2, "Error: inconsistent values with what is to be expected")
    End Sub

    '''<summary>
    '''A test for Calculate
    '''</summary>
    <TestMethod()> _
    Public Sub CalculateTest()
        Dim target As clsCharges = New clsCharges(10.0, 10.0) ' TODO: Initialize to an appropriate value
        Dim actual As Decimal
        Dim expected As Decimal = 22.4

        target.Calculate()
        actual = target.GetTotal
        Assert.AreEqual(actual, expected, "Error: inconsistent values with what is to be expected")
    End Sub
End Class
